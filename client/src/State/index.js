import { createGlobalState } from './GlobalState';
import { getStatusBarHeight } from '../utils/status-bar-height';
import MatchesService from '../routes/AppShell/PagerPages/MatchesPage/MatchesService';
import CompassService from '../routes/AppShell/PagerPages/CompassPage/CompassService';
import RequestsService from '../routes/AppShell/PagerPages/RequestsPage/RequestsService';
import PostsService from '../routes/AppShell/OverlayPages/Profile/PostsService';
import ChatsService from '../routes/AppShell/PagerPages/MessengerPage/ChatsService';

export const useGlobalState = createGlobalState({ user: null });
export const useMainPointState = createGlobalState({ mainPoint: null });
export const useMatchesState = createGlobalState({ matches: null });
export const useRequestsState = createGlobalState({ requests: null });
export const useChatsState = createGlobalState({ chats: null });
export const usePostsState = createGlobalState({ posts: null });
export const useGlobalPadding = createGlobalState(getStatusBarHeight());
export const useGlobalTabIndex = createGlobalState({ index: 1 });
export const useGlobalNotifications = createGlobalState({});

export const globalStateMethods = {
  async fetchMatches(setState) {
    setState({ matches: await MatchesService.getMatches() });
  },
  async fetchCompass(setState) {
    setState({ mainPoint: await CompassService.getCompassPoint() });
  },
  async fetchRequests(setState) {
    setState({ requests: await RequestsService.getRequests() });
  },
  async fetchChats(setState) {
    let chats = await ChatsService.getChats();
    setState({ chats });
  },
  async fetchPosts(setState) {
    setState({ posts: await PostsService.getPosts() });
  }
};
