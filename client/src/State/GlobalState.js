import { useEffect, useState } from 'react';

function setState(changes) {
  this.state = { ...this.state, ...changes };
  this.listeners.forEach(listener => listener(this.state));
}

const customHook = setState => function (withoutSetter = false) {
  if (!withoutSetter) {
    const [_, listener] = useState();

    useEffect(() => {
      this.listeners.push(listener);

      return () => {
        this.listeners = this.listeners.filter(elem => elem !== listener);
      };
    }, []);
  }

  return [this.state, setState];
};

export const createGlobalState = initialState => {
  const store = { state: initialState, listeners: [] };

  return customHook(setState.bind(store)).bind(store);
};
