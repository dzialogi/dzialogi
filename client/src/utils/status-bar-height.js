import { Dimensions, Platform, StatusBar } from 'react-native';

const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const { height: W_HEIGHT, width: W_WIDTH } = Dimensions.get('window');

let isIPhoneX = Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS &&
  (W_WIDTH === X_WIDTH && W_HEIGHT === X_HEIGHT || W_WIDTH === XSMAX_WIDTH && W_HEIGHT === XSMAX_HEIGHT);

export function getStatusBarHeight(skipAndroid) {
  return Platform.select({
    ios: isIPhoneX ? 44 : 20,
    android: skipAndroid ? 0 : StatusBar.currentHeight,
    default: 0
  });
}

export function getWindowHeight(paddingTop) {
  let screenHeight = Dimensions.get('screen').height;
  let windowHeight = Dimensions.get('window').height;

  if (Platform.OS === 'ios') return windowHeight;

  let hasNavigation = false, isNotch = false, hasNavigationAndNotch = false;

  if (screenHeight !== windowHeight) {
    hasNavigation = screenHeight - windowHeight === 48;
    isNotch = screenHeight - windowHeight < 48;
    hasNavigationAndNotch = screenHeight - windowHeight >= 48;
  }

  if (hasNavigation)
    return windowHeight;

  if (isNotch)
    return windowHeight + paddingTop;

  if (hasNavigationAndNotch)
    return windowHeight + paddingTop;

  return windowHeight;
}
