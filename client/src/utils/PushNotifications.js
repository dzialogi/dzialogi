import { AppState, AsyncStorage, Platform } from 'react-native';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import MessengerService from '../routes/AppShell/OverlayPages/Messenger/MessengerService';
import NavigationService from '../services/NavigationService';
import { withLoading } from './loader';
import { NotificationsService, PUSH_NOTIFICATION_TOKEN } from '../services/NotificationsService';

let appState = AppState.currentState;

let subscribers = {};
let idCounter = 0;

export function subscribeToNotificationChannel(channel, callBack) {
  if (!subscribers[channel]) subscribers[channel] = {};
  const handle = idCounter++;
  subscribers[channel][handle] = callBack;
  return handle;
}

export function unsubscribeFromNotification(handle) {
  for (let channel in subscribers) {
    if (subscribers.hasOwnProperty(channel) && subscribers[channel][handle]) {
      delete subscribers[channel][handle];
      return;
    }
  }
}

export const init = async () => {
  AppState.addEventListener('change', nextAppState => appState = nextAppState);
  let token = await AsyncStorage.getItem(PUSH_NOTIFICATION_TOKEN, null);
  if (!token) {
    await registerForPushNotificationsAsync();
  }
  if (Platform.OS === 'android') {
    Notifications.createChannelAndroidAsync('pushChannel', {
      name: 'pushChannel',
      priority: 'max',
      vibrate: [0, 250, 250, 250],
      sound: true,
      badge: true
    }).then().catch();
  }
  NotificationsService.registerToken().then();
  Notifications.addListener(notification => {
    if (notification && notification.data && notification.data.channel) {
      Object.values(subscribers[notification.data.channel]).forEach(callBack => callBack(notification));
    }
    if (appState === 'active' && notification.origin === 'received') {
      // Notifications.dismissNotificationAsync(notification.notificationId).then();
    } else if (notification.origin === 'received') {
    } else {
      ({
        async chat() {
          const { chatId } = notification.data;
          const chat = await withLoading(MessengerService.getChat(chatId));
          NavigationService.navigation.navigate('Messenger', { chat });
        }
      })[notification.data.channel]();
    }
  });
};


async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== 'granted') {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== 'granted') {
    return;
  }
  try {
    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
    // Save token for further uses
    await AsyncStorage.setItem(PUSH_NOTIFICATION_TOKEN, token);
  } catch (e) {
  }
}