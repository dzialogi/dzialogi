import { YellowBox } from 'react-native';
import * as Font from 'expo-font';
import { init as pushNotificationsInit } from './PushNotifications';

Number.prototype.map = function (callBack) {
  return new Array(+this).fill(0).map((v, i) => callBack(i));
};

YellowBox.ignoreWarnings([
  'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?',
  'Warning: Failed prop type: Invalid props.style key `0` supplied to `ParsedText',
]);
console.ignoredYellowBox = ['Remote debugger'];

const DonePreparations = new Promise(resolve => {
  let i = 0;
  const resolver = () => (++i === 2) && resolve(true);
  Font.loadAsync({ Roboto: require('../../node_modules/native-base/Fonts/Roboto.ttf') }).then(resolver);
  pushNotificationsInit().then(resolver);
});

export default DonePreparations;