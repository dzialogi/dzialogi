export function getPartner(chat, user) {
  let partnerUser = chat.user1;
  if (chat.user1._id === user._id)
    partnerUser = chat.user2;

  return partnerUser;
}

export function getAgreement(chat, user) {
  let agreement = chat.agreement1;
  if (chat.user2._id === user._id)
    agreement = chat.agreement2;

  return agreement;
}

export function setAgreement(chat, user, agreement) {
  if (chat.user1._id === user._id)
    chat.agreement1 = agreement;
  else
    chat.agreement2 = agreement;

  return chat;
}

export function bothAgreed(chat) {
  return chat.agreement1 && chat.agreement2;
}
