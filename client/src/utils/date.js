export function toDateString(date) {
  return [fill(date.getDate()),
    fill(date.getMonth() + 1),
    date.getFullYear()].join('/');
}

export function toTimeString(date) {
  return [fill(date.getHours()),
    fill(date.getMinutes()),
    fill(date.getSeconds())].join(':');
}

export function toDateTimeString(date) {
  return toDateString(date)
    + ' ' +
    toTimeString(date);
}

function fill(str, digits = 2) {
  str = str + '';
  while (str.length < digits)
    str = '0' + str;
  return str;
}


