import React, { useEffect, useState } from 'react';
import { BackHandler, Image, View } from 'react-native';
import { LoaderIcon } from '../../assets';
import NavigationService from '../services/NavigationService';


export const LoaderService = {
  startLoading: () => {
  },
  stopLoading: () => {
  }
};
export const withLoading = async func => {
  try {
    LoaderService.startLoading();
    if (func instanceof Promise) return await func;
    const res = func();
    if (res instanceof Promise) return await res;
    return res;
  } finally {
    LoaderService.stopLoading();
  }
};

const LoaderStyles = {
  position: 'absolute',
  backgroundColor: 'rgba(255, 255, 255, 0.7)',
  height: '100%',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  Image: {
    transform: [{
      scaleX: 0.6
    }, {
      scaleY: 0.6
    }]
  }
};

let savedShowLoader = 0;

export const Loader = ({ alwaysShow = false }) => {
  if (alwaysShow) {
    return <View style={LoaderStyles}>
      <Image source={LoaderIcon} style={LoaderStyles.Image}/>
    </View>;
  }
  let [showLoader, setShowLoader] = useState(savedShowLoader);
  useEffect(() => {
    LoaderService.startLoading = () => {
      savedShowLoader++;
      setShowLoader(savedShowLoader);
    };
    LoaderService.stopLoading = () => {
      savedShowLoader = Math.max(savedShowLoader - 1, 0);
      setShowLoader(savedShowLoader);
    };
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (!savedShowLoader) {
        NavigationService.navigation.goBack(null);
      }
      return true;
    });
    return () => backHandler.remove();
  }, []);
  return showLoader > 0 || alwaysShow ? <View style={LoaderStyles}>
    <Image source={LoaderIcon} style={LoaderStyles.Image}/>
  </View> : <></>;
};
