import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const Icon = props => <Ionicons {...props} name={`${(Platform.OS === 'ios' ? 'ios' : 'md')}-${props.name}`}/>;

export default Icon;