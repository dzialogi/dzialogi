import React from 'react';
import { Text } from 'react-native';

const DzialogiDefaultTextStyles = {
  fontFamily: 'Roboto',
  lineHeight: 16
};

const DzialogiText = ({ children, style = {}, ...props }) => {
  return (
    <Text
      style={[DzialogiDefaultTextStyles, style, { lineHeight: style.lineHeight || style.fontSize + 2 || DzialogiDefaultTextStyles.lineHeight }]} {...props}>
      {children}
    </Text>
  );
};

export default DzialogiText;