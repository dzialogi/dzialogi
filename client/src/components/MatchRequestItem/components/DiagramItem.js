import React from 'react';
import { View } from 'react-native';
import DzialogiText from '../../DzialogiText';

const Style = {
  flex: 1,
  Title: {
    fontSize: 18,
    textAlign: 'center',
    padding: 5
  },
  Diagram: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    Label: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#2cb5d2'
    },
    Progress: {
      position: 'absolute',
      width: '100%',
      bottom: 0
    }
  }
};

const DiagramItem = ({ value, color, title }) => {
  value = (value * 100).toFixed(1);
  return (
    <View style={Style}>
      <DzialogiText style={Style.Title}>{title}</DzialogiText>
      <View style={Style.Diagram}>
        <View style={[Style.Diagram.Progress, { height: value + '%', backgroundColor: color }]}/>
        <DzialogiText style={Style.Diagram.Label}>{value}%</DzialogiText>
      </View>
    </View>
  );
};

export default DiagramItem;
