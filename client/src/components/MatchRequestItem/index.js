import React, { useEffect, useState } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { CloseIcon, HandIcon, TrashIcon, UserIcon } from '../../../assets/icons';
import UsersService from '../../services/UsersService';
import { getUserAvatar } from '../../utils/user';
import DzialogiText from '../DzialogiText';
import NavigationService from '../../services/NavigationService';

const Style = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,
  elevation: 3,
  backgroundColor: 'white',
  marginHorizontal: '5%',
  marginVertical: 10,
  borderRadius: 10,
  width: '90%',
  padding: 30,
  TopBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%'
  },
  UserBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    maxWidth: '35%',
    Avatar: {
      width: 50,
      height: 50,
      borderRadius: 25,
      marginRight: 20
    },
    UserName: {
      fontSize: 18,
      fontWeight: 'bold'
    },
  },
  Matches: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between',
    Item: {
      width: '48%',
      Value: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
      },
      Progress: {
        height: 10,
        backgroundColor: '#ccc',
        borderRadius: 5
      },
      Title: {
        marginBottom: 5
      }
    }
  },
  Actions: {
    flexDirection: 'row',
    marginTop: -10,
    IconWrapper: {
      alignItems: 'center',
      justifyContent: 'center',
      height: 50,
      width: 50,
      borderRadius: 25,
      marginRight: 5,
      backgroundColor: '#f5f5f5',
      elevation: 3,
    },
    Icon: {
      height: 20,
      width: 20
    }
  }

};

export default ({ match, onReject, onAccept, rejectIcon = CloseIcon, userIcon = UserIcon, acceptIcon = HandIcon, style }) => {
  acceptIcon = acceptIcon || HandIcon;

  const [matchUser, setMatchUser] = useState({});

  const fetchUser = async () => {
    if (!match) return;
    let matchUser = await UsersService.getUserByUsername(match.username);
    setMatchUser(matchUser);
  };

  useEffect(() => {
    fetchUser().then();
  }, [match]);

  let politicalMatch = Math.floor(match.politicalMatch * 100);
  let personalMatch = Math.floor(match.personalMatch * 100);

  return (<View style={[Style, style]}>
    <View style={Style.TopBlock}>
      <TouchableOpacity style={Style.UserBlock}
                        onPress={() => NavigationService.navigation.navigate('Profile', { username: match.username })}>
        <Image source={{ uri: getUserAvatar(matchUser) }} style={Style.UserBlock.Avatar}/>
        <DzialogiText style={Style.UserBlock.UserName} numberOfLines={1}>
          {match.username}
        </DzialogiText>
      </TouchableOpacity>
      <View style={Style.Actions}>
        {onAccept && <View>
          <TouchableOpacity style={Style.Actions.IconWrapper} onPress={() => onAccept(match)}>
            {/*<Icon name="chatboxes" size={25} color="#555"/>*/}
            <Image source={acceptIcon} style={Style.Actions.Icon}/>
          </TouchableOpacity>
        </View>}
        {onReject && <View>
          <TouchableOpacity style={[Style.Actions.IconWrapper, { backgroundColor: '#f5364e' }]}
                            onPress={() => onReject(match)}>
            <Image source={TrashIcon} style={Style.Actions.Icon}/>
          </TouchableOpacity>
        </View>}
      </View>
    </View>
    <View style={Style.Matches}>
      <View style={Style.Matches.Item}>
        <DzialogiText style={Style.Matches.Item.Value}>{personalMatch}%</DzialogiText>
        <DzialogiText style={Style.Matches.Item.Title}>პიროვნული</DzialogiText>
        <View style={[Style.Matches.Item.Progress, { width: personalMatch + '%', backgroundColor: '#a089fd' }]}/>
      </View>
      <View style={Style.Matches.Item}>
        <DzialogiText style={Style.Matches.Item.Value}>{politicalMatch}%</DzialogiText>
        <DzialogiText style={Style.Matches.Item.Title}>პოლიტიკური</DzialogiText>
        <View style={[Style.Matches.Item.Progress, { width: politicalMatch + '%', backgroundColor: '#d0021b' }]}/>
      </View>
    </View>
  </View>);
}
