import AbstractService from './AbstractService';
import { WSSubject } from './WSSubject';

const UsersService = new (class UsersService extends AbstractService {
  subject = new WSSubject('users').subscribe();

  async getUserById(userId) {
    try {
      return await this.subject.emit('getUserById', userId);
    } catch (e) {
      return [];
    }
  }

  async getUserByUsername(username) {
    try {
      return await this.subject.emit('getUserByUsername', username);
    } catch (e) {
      return [];
    }
  }

  async updateUser(userId, firstName, lastName, password, confirmPassword, avatar) {
    try {
      return await this.subject.emit('updateUser', { userId, firstName, lastName, password, confirmPassword, avatar });
    } catch (e) {
      return [];
    }
  }


})();
export default UsersService;
