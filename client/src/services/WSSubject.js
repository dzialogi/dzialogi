import Socket from './SocketService';
import { AsyncStorage } from 'react-native';
import NavigationService from './NavigationService';

export const AUTH_TOKEN = 'authToken';

const DEFAULT_SUBSCRIPTION_NAME = 'defaultSubscription';

const openSubjects = [];
Socket.on('reconnect', () => openSubjects.forEach(s => s.startListening()));
const requestTimeout = 600000;
const timeOutError = obj => new Error(`Request ${obj.name} timed out after waiting for ${requestTimeout / 1000} seconds.`);
let id = 0;
let shouldAlert = true;

const defaultCallBacks = subject => ({
  set(data) {
    // set new data into stored data
    // ex: const [store, setStoreState] = useState([]);
    // ex: ...
    // ex: setStoreState({ data });
  },
  insert(data) {
    // insert data into stored data
    // ex: const [store, setStoreState] = useState([]);
    // ex: ...
    // ex: setStoreState({ data: store.data.concat(data) });
  },
  update(data) {
    // update stored data according to given data
    // ex: const [store, setStoreState] = useState([]);
    // ex: ...
    // ex: setStoreState({ data: store.data.map(item => data.find(newItem => newItem._id === item._id) || item) });
  },
  delete(data) {
    // update stored data according to given data
    // ex: const [store, setStoreState] = useState([]);
    // ex: ...
    // ex: setStoreState({ data: store.data.filter(item => !data.find(newItem => newItem._id === item._id)) });
  },
  response(data, status, responseId) {
    if (!subject.responses[responseId]) return;
    if (status === 200)
      subject.responses[responseId].resolve(data);
    else
      subject.responses[responseId].reject({ status, data });
    delete subject.responses[responseId];
  },
});

export class WSSubject {
  // v1
  constructor(subject, dynamicName = DEFAULT_SUBSCRIPTION_NAME) {
    this.subject = subject;
    this.dynamicName = dynamicName;
    this.callBacks = defaultCallBacks(this);
    this.responses = {};
    this.interval = setInterval(() => Object.keys(this.responses).forEach(requestId =>
      (this.responses[requestId].time < (new Date().getTime() - requestTimeout)) ?
        this.responses[requestId].reject(timeOutError(this.responses[requestId])) && delete this.responses[requestId] :
        null
    ), 10000);
    this.startListening();
    openSubjects.push(this);
    Socket.on(this.subject, this.subscription);
  }

  subscribe(callBacks) {
    this.callBacks = {
      ...this.callBacks,
      ...callBacks
    };
    return this;
  }

  async unsubscribe() {
    await this.emit('unsubscribe', this.dynamicName, true);
    Socket.removeListener(this.subject, this.subscription);
    clearInterval(this.interval);
  }

  startListening = () => {
    this.emit('subscribe', this.dynamicName, false).then().catch();
  };

  subscription = (response) => {
    if (response.action === 'response' || this.dynamicName === response.dynamicName)
      this.callBacks[response.action](response.data, response.status, response.id);
  };

  async emitter(action, data, hasResponse = true) {
    const requestId = id++;
    const call = async () =>
      Socket.emit(this.subject, {
        id: requestId,
        action,
        data,
        token: 'Bearer ' + await AsyncStorage.getItem(AUTH_TOKEN, null)
      });
    if (hasResponse) {
      return await new Promise(async (resolve, reject) => {
        call().then();
        this.responses[requestId] = {
          resolve,
          reject,
          time: new Date().getTime(),
          name: `${this.subject}.${action}.${requestId}`
        };
      });
    } else {
      call().then();
    }
  }

  async emit(action, data, hasResponse = true) {
    try {
      return await this.emitter(action, data, hasResponse);
    } catch (e) {
      if (e.status === 403 && !!await AsyncStorage.getItem(AUTH_TOKEN, null) && shouldAlert) {
        shouldAlert = false;
        await WSSubject.logout();
        alert(e.data && e.data.message || e.message || e.error && e.error.message);
        shouldAlert = true;
      }
      throw e;
    }
  }

  static async logout() {
    await AsyncStorage.removeItem(AUTH_TOKEN);
    if (NavigationService.navigation)
      NavigationService.navigation.navigate('AuthLoadingScreen');
  }

  // v2
  // constructor(subject, callBacks) {
  //   this.subject = subject;
  //   this.callBacks = callBacks || defaultCallBacks;
  //   this.startListening();
  // }
  //
  // startListening = () => {
  //   Socket.on(this.subject, (response) => {
  //     this.callBacks[response.action](response.data);
  //   });
  // };

  // v3
  // constructor(subject) {
  //   this.subject = subject;
  //   this.callBacks = { 0: defaultCallBacks };
  // }
  //
  // subscribe(callBacks) {
  //   const subscriptionId = Object.keys(this.callBacks).length;
  //   this.callBacks[subscriptionId] = callBacks;
  //   this.startListening();
  //   return subscriptionId;
  // }
  //
  // startListening = () => {
  //   Socket.on(this.subject, (response) => {
  //     Object.values(this.callBacks).forEach(callBack => callBack && callBack[response.action](response.data));
  //   });
  // };
  //
  // unsubscribe(subscriptionId) {
  //   delete this.callBacks[subscriptionId];
  // }
}