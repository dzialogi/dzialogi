import { AUTH_TOKEN, WSSubject } from './WSSubject';
import { AsyncStorage } from 'react-native';

export const PUSH_NOTIFICATION_TOKEN = 'pushNotificationToken';

export const NotificationsService = (new class NotificationsService {
  subject = new WSSubject('pushNotification').subscribe();

  async registerToken() {
    try {
      return await this.subject.emit('registerToken', await AsyncStorage.getItem(PUSH_NOTIFICATION_TOKEN, null));
    } catch (e) {
    }
  }

  async registerUser() {
    try {
      return await this.subject.emit('registerUser', {
        notificationToken: await AsyncStorage.getItem(PUSH_NOTIFICATION_TOKEN, null),
        authToken: await AsyncStorage.getItem(AUTH_TOKEN, null)
      });
    } catch (e) {
    }
  }

  async unregisterUser() {
    try {
      return await this.subject.emit('unregisterUser', await AsyncStorage.getItem(PUSH_NOTIFICATION_TOKEN, null));
    } catch (e) {
    }
  }
});