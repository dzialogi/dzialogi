import AbstractService from './AbstractService';
import { WSSubject } from './WSSubject';

const GlobalService = new (class GlobalService extends AbstractService {
  subject = new WSSubject('global').subscribe({});

  dynamicSubject;
  chatsUpdated;

  async setDynamic(dynamicName) {
    await this.unsubscribeDynamic();
    this.dynamicSubject = new WSSubject('global', dynamicName).subscribe({
      chatsUpdated: chats => {
        if (this.chatsUpdated)
          this.chatsUpdated(chats);
      }
    });
  }

  async unsubscribeDynamic() {
    if (this.dynamicSubject) {
      try {
        await this.dynamicSubject.unsubscribe();
      } catch (e) {
      }
      this.dynamicSubject = null;
    }
  }

})();
export default GlobalService;
