let NavigationService = {
  navigation: null
};

export const setNavigation = navigation => NavigationService.navigation = navigation;

export default NavigationService;