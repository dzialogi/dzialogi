import io from 'socket.io-client';
import config from '../../Config';

const Socket = io(config.apiUrl);

export default Socket;