import React, { useEffect } from 'react';
import AuthService from './Auth/AuthService';
import { useGlobalState } from '../State';
import { Loader } from '../utils/loader';
import NavigationService from '../services/NavigationService';
import { NotificationsService } from '../services/NotificationsService';

const AuthLoadingScreen = ({ navigation }) => {
  const [, setState] = useGlobalState();
  NavigationService.navigation = navigation;
  useEffect(() => {
    (async () => {
      try {
        const authToken = await AuthService.getAuthToken();
        const user = await AuthService.getLoggedUser();
        setState({ user });
        if (['P', 'N'].includes(user && user.userStatus))
          navigation.navigate('Quiz', { quizType: 'Political' });
        else
          navigation.navigate(authToken ? 'AppShell' : 'Login');
        if (authToken)
          NotificationsService.registerUser().then();
        else
          NotificationsService.unregisterUser().then();
      } catch (e) {
        navigation.navigate('Login');
      }
    })();
  }, []);

  return <Loader alwaysShow={true}/>;
};

export default AuthLoadingScreen;
