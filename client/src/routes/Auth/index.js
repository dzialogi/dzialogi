import { createStackNavigator } from 'react-navigation';

import Login from './Login';
import Register from './Register';

export default createStackNavigator(
  {
    Login,
    Register
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      header: null
    }
  }
);