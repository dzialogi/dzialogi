import AbstractService from '../../services/AbstractService';
import { AUTH_TOKEN, WSSubject } from '../../services/WSSubject';
import { AsyncStorage } from 'react-native';
import { NotificationsService } from '../../services/NotificationsService';


const AuthService = new (class AuthService extends AbstractService {
  subject = new WSSubject('auth').subscribe({});

  async register(username, password) {
    const resp = await this.subject.emit('register', { username, password });
    await this.setAuthToken(resp.token);
    return resp.user;
  }

  async login(username, password) {
    const resp = await this.subject.emit('login', { username, password });
    await this.setAuthToken(resp.token);
    return resp.user;
  }

  async logout() {
    NotificationsService.unregisterUser().then();
    await WSSubject.logout();
  }

  async getLoggedUser() {
    return (await this.subject.emit('getLoggedUser')).user;
  }

  removeAuthToken() {
    return AsyncStorage.removeItem(AUTH_TOKEN);
  }

  async getAuthToken() {
    return await AsyncStorage.getItem(AUTH_TOKEN, null);
  }

  async setAuthToken(authToken) {
    await AsyncStorage.setItem(AUTH_TOKEN, authToken);
  }
})();
export default AuthService;
