import React, { useEffect, useState } from 'react';
import { Animated, Image, Keyboard, KeyboardAvoidingView, Platform, TextInput, View } from 'react-native';
import { AppIcon } from '../../../../assets';
import { Button } from 'react-native-elements';
import loginStyle from './style';
import AuthService from '../AuthService';
import { useGlobalState } from '../../../State';
import DzialogiText from '../../../components/DzialogiText';
import { withLoading } from '../../../utils/loader';

const Index = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [logoHeight,] = useState(new Animated.Value(0));
  const [, setState] = useGlobalState();
  let keyboardDidHideListener, keyboardDidShowListener;


  useEffect(() => {
    animate();
    keyboardDidHideListener = Keyboard.addListener(
      Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide',
      () => animate(),
    );
    keyboardDidShowListener = Keyboard.addListener(
      Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow',
      () => animate(true),
    );
    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  const animate = (close = false) => {
    return new Promise(resolve => {
      Animated.timing(logoHeight, {
        toValue: close ? 0 : loginStyle.logoStyleWrapper.height,
        duration: 400
      }).start(resolve);
    });
  };

  const login = async () => {
    try {
      if (!username) {
        return alert('შეიყვანეთ მომხმარებლის სახელი');
      }
      if (!password) {
        return alert('შეიყვანეთ პაროლი');
      }
      const user = await withLoading(AuthService.login(username, password));
      setState({ user });
      props.navigation.navigate('AuthLoadingScreen');
    } catch (e) {
      alert(e.data && e.data.message || e.message || e.error && e.error.message || e.data && e.data.name);
    }
  };

  const navigateRegister = () => {
    props.navigation.navigate('Register');
  };

  return <KeyboardAvoidingView style={loginStyle} behavior="padding" enabled>
    <View style={loginStyle.wrapper}>
      <Animated.View style={{ ...loginStyle.logoStyleWrapper, height: logoHeight }}>
        <Image source={AppIcon} style={loginStyle.logoStyle}/>
      </Animated.View>
      <DzialogiText style={loginStyle.titleStyle}>ძიალოგი</DzialogiText>
      <TextInput style={loginStyle.inputStyle}
                 onChangeText={username => setUsername(username)}
                 value={username}
                 placeholder='მომხმარებელი'/>
      <TextInput style={loginStyle.inputStyle}
                 onChangeText={password => setPassword(password)}
                 value={password}
                 placeholder='პაროლი' secureTextEntry={true}/>
      <View style={loginStyle.buttonsStyle}>
        <Button
          containerStyle={{ ...loginStyle.buttonsStyle.buttonStyle, ...loginStyle.buttonsStyle.firstButtonStyle }}
          onPress={() => login()} title='შესვლა'/>
        <Button containerStyle={loginStyle.buttonsStyle.buttonStyle}
                onPress={() => navigateRegister()} title='რეგისტრაცია'/>
      </View>
    </View>
  </KeyboardAvoidingView>;
};

export default Index;
