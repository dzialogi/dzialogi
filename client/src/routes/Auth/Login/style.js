export default {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#F3F6FF',
  wrapper: {
    verticalAlign: 'center'
  },
  logoStyleWrapper: {
    width: 310,
    height: 310,
    overflow: 'hidden'
  },
  logoStyle: {
    width: 270,
    height: 270,
    margin: 20
  },
  titleStyle: {
    textAlign: 'center',
    margin: 30,
    fontSize: 40
  },
  inputStyle: {
    backgroundColor: 'white',
    width: 300,
    marginBottom: 20,
    padding: 10,
    borderRadius: 5
  },
  buttonsStyle: {
    flexDirection: 'row',
    width: 300,
    buttonStyle: {
      flex: 1
    },
    firstButtonStyle: {
      marginRight: 10
    }
  }
};
