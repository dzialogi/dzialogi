import React, { useState } from 'react';
import { KeyboardAvoidingView, TextInput, TouchableOpacity, View } from 'react-native';
import { Button } from 'react-native-elements';
import loginStyle from './style';
import { Icon } from '../../../components';
import AuthService from '../AuthService';
import { useGlobalPadding, useGlobalState } from '../../../State';
import DzialogiText from '../../../components/DzialogiText';
import { withLoading } from '../../../utils/loader';

const Index = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [repPassword, setRepPassword] = useState('');
  const [, setState] = useGlobalState();
  const [padding,] = useGlobalPadding();

  const register = async () => {
    try {
      if (!username) {
        return alert('შეიყვანეთ მომხმარებლის სახელი');
      }
      if (!password) {
        return alert('შეიყვანეთ პაროლი');
      }
      if (password !== repPassword) {
        return alert('განმეორებითი პაროლი არ ემთხვევა');
      }
      const user = await withLoading(AuthService.register(username, password));
      setState({ user });
      props.navigation.navigate('AuthLoadingScreen');
    } catch (e) {
      alert(e.data && e.data.message || e.message || e.error && e.error.message);
    }
  };

  return <KeyboardAvoidingView style={{ ...loginStyle, marginTop: padding }} behavior="padding" enabled>
    <TouchableOpacity style={loginStyle.backButtonStyle} onPress={() => props.navigation.pop(1)}>
      <Icon name="arrow-back" size={32} color="#555"/>
    </TouchableOpacity>
    <DzialogiText style={loginStyle.titleStyle}>ძიალოგი</DzialogiText>
    <TextInput style={loginStyle.inputStyle}
               onChangeText={username => setUsername(username)}
               value={username}
               placeholder='მომხმარებელი'/>
    <TextInput style={loginStyle.inputStyle}
               onChangeText={password => setPassword(password)}
               value={password}
               placeholder='პაროლი' secureTextEntry={true}/>
    <TextInput style={loginStyle.inputStyle}
               onChangeText={repPassword => setRepPassword(repPassword)}
               value={repPassword}
               placeholder='განმეორებითი პაროლი' secureTextEntry={true}/>
    <View style={loginStyle.buttonsStyle}>
      <Button
        containerStyle={loginStyle.buttonsStyle.buttonStyle}
        onPress={() => register().then()} title='რეგისტრაცია'/>
    </View>
  </KeyboardAvoidingView>;
};

export default Index;
