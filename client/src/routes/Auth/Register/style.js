export default {
  flex: 1,
  alignItems: 'center',
  backgroundColor: '#F3F6FF',
  backButtonStyle: {
    position: 'absolute',
    zIndex: 1,
    padding: 20,
    alignSelf: 'flex-start'
  },
  logoStyle: {
    width: 270,
    height: 270,
    margin: 20
  },
  titleStyle: {
    margin: 30,
    fontSize: 40
  },
  inputStyle: {
    backgroundColor: 'white',
    width: 300,
    marginBottom: 20,
    padding: 10,
    borderRadius: 5
  },
  buttonsStyle: {
    flexDirection: 'row',
    width: 300,
    buttonStyle: {
      flex: 1
    }
  }
};
