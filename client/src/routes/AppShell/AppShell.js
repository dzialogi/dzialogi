import React, { useEffect, useState } from 'react';
import { Dimensions, Platform, View } from 'react-native';
import { Notifications } from 'expo';
import { SceneMap, TabView } from 'react-native-tab-view';
import TabBar from './components/TabBar';
import { RouterNotifications, RouterViews, Routes } from './routes';
import {
  globalStateMethods,
  useChatsState,
  useGlobalNotifications,
  useGlobalPadding,
  useGlobalState,
  useGlobalTabIndex,
  useMainPointState,
  useMatchesState,
  usePostsState,
  useRequestsState
} from '../../State';
import { setNavigation } from '../../services/NavigationService';
import { AppShellStyle } from './tabBarStyle';
import Icon from '../../components/Icon';
import { withLoading } from '../../utils/loader';
import GlobalService from '../../services/GlobalService';
import { subscribeToNotificationChannel, unsubscribeFromNotification } from '../../utils/PushNotifications';


const AppShell = ({ navigation, tab }) => {
  const [index, setIndex] = useGlobalTabIndex();
  const [routes,] = useState(Routes.map(route => ({ ...route, navigation })));
  const [paddingTop, setPaddingTop] = useGlobalPadding();
  const [, setMatches] = useMatchesState();
  const [, setCompass] = useMainPointState();
  const [, setRequests] = useRequestsState();
  const [, setChats] = useChatsState();
  const [, setPosts] = usePostsState();
  const [globalState, setGlobalState] = useGlobalState();
  const [notifications, setNotifications] = useGlobalNotifications();

  setNavigation(navigation);
  useEffect(() => {
    withLoading(() => globalStateMethods.fetchMatches(setMatches)).then();
    withLoading(() => globalStateMethods.fetchCompass(setCompass)).then();
    withLoading(() => globalStateMethods.fetchRequests(setRequests)).then();
    withLoading(() => globalStateMethods.fetchChats(setChats)).then();
    withLoading(() => globalStateMethods.fetchPosts(setPosts)).then();
  }, []);

  useEffect(() => {
    const handles = RouterNotifications.map((channel, i) =>
      subscribeToNotificationChannel(channel, notification => {
        if (i === index.index) return;
        if (!notifications[i]) notifications[i] = [];
        notifications[i].push(notification);
        setNotifications(notifications);
      })
    );
    return () => handles.forEach(unsubscribeFromNotification);
  }, [notifications]);

  useEffect(() => {
    if (!notifications[index.index]) notifications[index.index] = [];
    notifications[index.index].forEach(({ notificationId }) => Notifications.dismissNotificationAsync(notificationId).then());
    notifications[index.index] = [];
    setNotifications(notifications);
  }, [index]);

  useEffect(() => {
    if (tab !== undefined) {
      setIndex(tab);
      withLoading(() => globalStateMethods.fetchRequests(setRequests)).then();
    }
  }, [tab]);

  const updateChats = (chats) => {
    setChats({ chats });
  };

  useEffect(() => {
    if (globalState.user && globalState.user._id) {
      GlobalService.setDynamic(globalState.user._id);
      GlobalService.chatsUpdated = updateChats;
    }
    return () => GlobalService.unsubscribeDynamic();
  }, [globalState]);

  return <View style={{ flex: 1 }}>
    <TabView
      navigationState={{ routes, ...index }}
      renderScene={SceneMap(RouterViews)}
      renderTabBar={props => <TabBar {...props}/>}
      tabBarPosition={'bottom'}
      onIndexChange={index => setIndex({ index })}
      initialLayout={{ width: Dimensions.get('window').width }}
      style={[AppShellStyle, { paddingTop }]}
    />
    <Icon
      name="menu"
      size={32}
      color="#aaa"
      style={{
        alignSelf: 'flex-start',
        zIndex: 1,
        margin: 20,
        marginTop: (Platform.OS === 'android' ? 14 : 7) + paddingTop,
        position: 'absolute'
      }}
      onPress={() => navigation.openDrawer()}
    />
  </View>;
};

AppShell.navigationOptions = () => ({
  title: 'ძიალოგი'
});

export default AppShell;
