import { CompassPage, MatchesPage, MessengerPage, NewsFeedPage, QuizPage, RequestsPage } from './PagerPages';
import { CompassIcon, HandIcon, MatchIcon, MessengerIcon, NewsFeedIcon, QuestionIcon } from '../../../assets/icons';
import { Keys } from './routesKeys';

export const Routes = [
  {
    key: Keys.NEWS_FEED,
    title: 'კედელი'
  },
  {
    key: Keys.COMPASS,
    title: 'კომპასი'
  },
  {
    key: Keys.QUIZ,
    title: 'ქუიზები'
  },
  {
    key: Keys.MATCHES,
    title: 'პოვნა'
  },
  {
    key: Keys.REQUESTS,
    title: 'მოწვევები'
  },
  {
    key: Keys.MESSENGER,
    title: 'ძიალოგები'
  },
];

export const RouterViews = {
  [Keys.NEWS_FEED]: NewsFeedPage,
  [Keys.COMPASS]: CompassPage,
  [Keys.QUIZ]: QuizPage,
  [Keys.MATCHES]: MatchesPage,
  [Keys.REQUESTS]: RequestsPage,
  [Keys.MESSENGER]: MessengerPage
};

export const RouterIcons = [
  NewsFeedIcon,
  CompassIcon,
  QuestionIcon,
  MatchIcon,
  HandIcon,
  MessengerIcon,
];

export const RouterNotifications = [
  'feed',
  'compass',
  'quiz',
  'match',
  'offer',
  'chat'
];