import React from 'react';
import { Image, View } from 'react-native';
import { TabBar } from 'react-native-tab-view';
import { RouterIcons } from '../routes';
import tabBarStyle from '../tabBarStyle';
import { useGlobalNotifications } from '../../../State';

export default props => {
  const [notifications,] = useGlobalNotifications();
  return <TabBar
    {...props}
    indicatorStyle={tabBarStyle.indicatorStyle}
    style={tabBarStyle}
    renderLabel={({ route }) =>
      <View style={tabBarStyle.iconWrapper}>
        <Image source={RouterIcons[route.key]} style={tabBarStyle.renderLabel}/>
        {notifications[route.key] && !!notifications[route.key].length &&
        <View style={tabBarStyle.notificationIndicator}/>}
      </View>
    }
  />;
};
