import { Image, View } from 'react-native';
import React from 'react';
import DzialogiText from '../../../components/DzialogiText';

const Style = {
  Main: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Icon: {
    width: 200,
    height: 200,
    marginBottom: 60
  },
  Text: {
    fontSize: 20,
    textAlign: 'center',
  }
};

export const NoDataPlaceholder = ({ icon, text, style = {} }) =>
  <View style={[Style.Main, style]}>
    <Image source={icon} style={Style.Icon}/>
    <DzialogiText style={Style.Text}>{text}</DzialogiText>
  </View>;
