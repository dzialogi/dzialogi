import React from 'react';
import { View } from 'react-native';
import AuthService from '../../../Auth/AuthService';

class LogOutItem extends React.Component {
  static navigationOptions = () => ({
    title: 'გასვლა'
  });

  componentDidMount() {
    AuthService.logout().then();
  }

  render() {
    return (
      <View/>
    );
  }
}

export default LogOutItem;
