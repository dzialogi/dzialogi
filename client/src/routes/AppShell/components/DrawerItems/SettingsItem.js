import React from 'react';
import { View } from 'react-native';
import { drawerItemStyle } from './style';
import DzialogiText from '../../../../components/DzialogiText';

class SettingsItem extends React.Component {
  static navigationOptions = () => ({
    title: 'პარამეტრები'
  });

  render() {
    return (
      <View style={drawerItemStyle}>
        <DzialogiText>
          settings
        </DzialogiText>
      </View>
    );
  }
}

export default SettingsItem;