import React from 'react';
import { ScrollView, View } from 'react-native';
import { DrawerItems } from 'react-navigation';
import DrawerHeader from './components/DrawerHeader';

const ContentComponent = (props) => {
  return <ScrollView>
    <View style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <DrawerHeader/>
      <DrawerItems {...props} />
    </View>
  </ScrollView>;
};

const styles = {
  container: {
    flex: 1,
  },
};

export default ContentComponent;
