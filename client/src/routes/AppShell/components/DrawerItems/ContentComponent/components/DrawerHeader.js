import React from 'react';
import { Image, ImageBackground, View } from 'react-native';
import { useGlobalPadding, useGlobalState } from '../../../../../../State';
import { DrawerHeaderStyle } from '../style';
import { getUserAvatar } from '../../../../../../utils/user';
import DzialogiText from '../../../../../../components/DzialogiText';

const DrawerHeader = () => {
  const [state,] = useGlobalState();
  const [paddingTop,] = useGlobalPadding();
  return (
    state.user ?
      <ImageBackground
        source={{ uri: 'https://www.setaswall.com/wp-content/uploads/2017/06/Material-Backgrounds-02-1920-x-1080.png' }}
        style={DrawerHeaderStyle}>
        <View style={[DrawerHeaderStyle.userIconStyle, { paddingTop }]}>
          {/*<Image source={ { uri: state.user.image } } style={DrawerHeaderStyle.userIconStyle.img}/>*/}
          <Image
            source={{ uri: getUserAvatar(state.user) }}
            style={DrawerHeaderStyle.userIconStyle.img}/>
        </View>
        <DzialogiText style={[DrawerHeaderStyle.userTextStyle, { paddingTop }]}>{state.user.username}</DzialogiText>
      </ImageBackground> : <View/>
  );
};

export default DrawerHeader;
