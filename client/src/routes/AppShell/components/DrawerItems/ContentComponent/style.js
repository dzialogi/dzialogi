export const DrawerHeaderStyle = {
  alignItems: 'center',
  flexDirection: 'row',
  padding: 10,
  paddingTop: 30,
  paddingBottom: 30,
  userIconStyle: {
    margin: 10,
    borderRadius: 25,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'black',
    shadowOpacity: 1.0,
    img: {
      height: 50,
      width: 50,
      borderRadius: 25
    }
  },
  userTextStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    flex: 1
  }
};
