import React, { useEffect, useState } from 'react';
import { TextInput, View } from 'react-native';
import { AppShellStyle } from '../tabBarStyle';

const Style = {
  padding: 10,
  zIndex: 2,
  backgroundColor: AppShellStyle.backgroundColor,
  SearchInput: {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 5,
    paddingLeft: 50,
  },
};

const SearchComponent = ({ data, getKey, onFilter }) => {
  const [text, setText] = useState('');
  const handleTextChange = term => {
    setText(term);
    onFilter(data && data.filter(item => getKey(item).trim().toLowerCase().includes(term.trim().toLowerCase())));
  };
  useEffect(() => {
    handleTextChange(text);
  }, [data]);
  return (
    <View style={Style}>
      <TextInput placeholder={'ძებნა...'} style={Style.SearchInput} onChangeText={handleTextChange} value={text}/>
    </View>
  );
};

export default SearchComponent;