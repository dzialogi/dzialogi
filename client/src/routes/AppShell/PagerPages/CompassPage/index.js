import React, { useEffect, useState } from 'react';
import Svg, { Circle, Line } from 'react-native-svg';
import { Animated, PanResponder, RefreshControl, ScrollView, View } from 'react-native';
import tabBarStyle from '../../tabBarStyle';
import { globalStateMethods, useGlobalPadding, useMainPointState } from '../../../../State';
import CompassService from './CompassService';
import { LinearGradient } from 'expo-linear-gradient';
import DzialogiText from '../../../../components/DzialogiText';
import { getWindowHeight } from '../../../../utils/status-bar-height';
import { CompassStyle } from './CompassStyle';
import MatchRequestItem from '../../../../components/MatchRequestItem';
import { MessageCircleIcon } from '../../../../../assets/icons';
import { withLoading } from '../../../../utils/loader';
import MatchesService from '../MatchesPage/MatchesService';
import MessengerService from '../../OverlayPages/Messenger/MessengerService';
import NavigationService from '../../../../services/NavigationService';

const SELECTION_BOTTOM = -20;

export default () => {
  const [{ mainPoint }, setState] = useMainPointState();
  const [refreshing, setRefreshing] = useState(false);
  const [selectedPointBottom,] = useState(new Animated.Value(-200));
  const [globalPadding,] = useGlobalPadding();
  const [selectedPoint, setSelectedPoint] = useState(null);
  CompassService.deleteMatch = username => {
    mainPoint.matches = mainPoint.matches.filter(match => match.username !== username);
    setState({ mainPoint });
  };
  CompassService.setMainPoint = setState;
  useEffect(() => {
    globalStateMethods.fetchCompass(setState);
  }, []);
  const onRefresh = async () => {
    try {
      setRefreshing(true);
      await globalStateMethods.fetchCompass(setState);
    } finally {
      setRefreshing(false);
    }
  };

  const onSelectPoint = point => {
    Animated.timing(selectedPointBottom, {
      toValue: SELECTION_BOTTOM,
      duration: 400
    }).start();
    setSelectedPoint(point);
  };

  const onHideSelectedPoint = () => {
    Animated.timing(selectedPointBottom, {
      toValue: -200,
      duration: 400
    }).start(() => setSelectedPoint(null));
  };

  const _panResponder = PanResponder.create({
    onMoveShouldSetResponderCapture: () => true,
    onMoveShouldSetPanResponderCapture: (evt, { dy }) => dy !== 0,
    onPanResponderMove: (evt, { dy }) => {
      Animated.timing(selectedPointBottom, {
        toValue: -dy + SELECTION_BOTTOM,
        duration: 0
      }).start();
    },
    onPanResponderRelease: (e, { vy, dy }) => {
      if (Math.abs(vy) >= 0.5 || Math.abs(dy) >= 100) {
        Animated.timing(selectedPointBottom, {
          toValue: dy < SELECTION_BOTTOM ? SELECTION_BOTTOM : -200,
          duration: 200
        }).start(() => dy > 0 && onHideSelectedPoint());
      } else {
        Animated.spring(selectedPointBottom, {
          toValue: SELECTION_BOTTOM,
          bounciness: 10
        }).start();
      }
    }
  });

  const removePoint = match => {
    mainPoint.matches = mainPoint.matches.filter(listMatch => listMatch.username !== match.username);
    setState({ mainPoint });
    onHideSelectedPoint();
  };

  const handleAccept = async match => {
    if (selectedPoint.isChat) {
      try {
        const chat = await withLoading(MessengerService.getChatByUsername(match.username));
        NavigationService.navigation.navigate('Messenger', { chat });
        onHideSelectedPoint();
      } catch (e) {

      }
    } else {
      try {
        const res = await withLoading(MatchesService.accept(match.username));
        removePoint(match);
        if (res.status === 'ACCEPTED') {
          globalStateMethods.fetchChats(setState);
        }
      } catch (e) {

      }
    }
  };

  const handleReject = async match => {
    try {
      const res = await withLoading(MatchesService.reject(match.username));
      if (res.status === 'ok')
        removePoint(match);
    } catch (e) {

    }
  };

  return <LinearGradient
    style={{ flex: 1 }}
    colors={['#576bf2', '#ad8eff']}
    locations={[0, 1]}
    start={[0.5, 0]}
    end={[0.5, 1]}
  >
    <View style={[CompassStyle.Circle, CompassStyle.Circle.Small]}/>
    <View style={[CompassStyle.Circle, CompassStyle.Circle.Medium]}/>
    <View style={[CompassStyle.Circle, CompassStyle.Circle.Big]}/>
    <View style={CompassStyle.MainLineXLabelWrapper}>
      <DzialogiText style={CompassStyle.MainLineXLabel}>არანაცი</DzialogiText>
      <DzialogiText style={CompassStyle.MainLineXLabel}>არაქოცი</DzialogiText>
    </View>
    <View style={CompassStyle.MainLineYLabelWrapper}>
      <DzialogiText style={CompassStyle.MainLineYLabel}>პროგრესივიზმი</DzialogiText>
      <DzialogiText style={CompassStyle.MainLineYLabel}>ტრადიციონალიზმი</DzialogiText>
    </View>
    <View style={CompassStyle.MainLineX}/>
    <View style={CompassStyle.MainLineY}/>
    <ScrollView
      scrollEnabled={false}
      contentContainerStyle={CompassStyle.ScrollView}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }>
      <Svg
        height={getWindowHeight(globalPadding) - tabBarStyle.renderLabel.height}
        width="100%"
        onPress={() => onHideSelectedPoint()}
      >
        {!!mainPoint && !!mainPoint.matches && !!mainPoint.matches.length && mainPoint.matches.map((point, id) => point.isConnected &&
          <Line
            x1={mainPoint.x + '%'}
            y1={mainPoint.y + '%'}
            x2={point.x + '%'}
            y2={point.y + '%'}
            r={1 + '%'}
            key={id + '%'}
            stroke={CompassStyle.linesColor}
            strokeWidth="2"/>)}
        {!!mainPoint && !!mainPoint.matches && !!mainPoint.matches.length && mainPoint.matches.map((point, id) =>
          [
            <Circle cx={point.x + '%'}
                    cy={point.y + '%'}
                    r={1 + '%'}
                    fill="#5074ff"
                    stroke="white"
                    strokeWidth="1"
                    key={id + 'circle'}/>,
            <Circle cx={point.x + '%'}
                    cy={point.y + '%'}
                    r={(selectedPoint && selectedPoint.username === point.username ? 5 : 3) + '%'}
                    fill="rgba(255,255,255,0.2)"
                    strokeWidth="0"
                    onPress={() => onSelectPoint(point)}
                    key={id + 'circleOutline'}/>
          ])
        }
        {!!mainPoint && !!mainPoint.x && !!mainPoint.y && [
          <Circle cx={mainPoint.x + '%'}
                  cy={mainPoint.y + '%'}
                  r={1.7 + '%'}
                  fill="#ff5800"
                  stroke="white"
                  strokeWidth="1"
                  key={'circle'}/>,
          <Circle cx={mainPoint.x + '%'}
                  cy={mainPoint.y + '%'}
                  r={5 + '%'}
                  fill="rgba(255,255,255,0.2)"
                  strokeWidth="0"
                  key={'circleOutline'}/>
        ]}
      </Svg>
    </ScrollView>
    {selectedPoint &&
    <Animated.View style={{ bottom: selectedPointBottom }} {..._panResponder.panHandlers}><MatchRequestItem
      style={CompassStyle.SelectionStyle}
      acceptIcon={selectedPoint.isChat ? MessageCircleIcon : undefined}
      onAccept={handleAccept}
      onReject={selectedPoint.isChat ? undefined : handleReject}
      match={selectedPoint}
    /></Animated.View>}
  </LinearGradient>;
};
