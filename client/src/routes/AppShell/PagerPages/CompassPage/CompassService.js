import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const MIN_RANGE = 15, MAX_RANGE = 85;
const MIN_VALUE = -100, MAX_VALUE = 100;
const MAX_MATCHES = 5;

function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

const CompassService = new (class CompassService extends AbstractService {
  deleteMatch;
  setMainPoint;
  subject = new WSSubject('compass').subscribe({
    delete: ({ username }) => this.deleteMatch(username),
    set: mainPoint => {
      this.setMainPoint({ mainPoint: this.transform(mainPoint, false) });
    }
  });

  async getCompassPoint() {
    try {
      const mainPoint = this.transform(await this.subject.emit('getCompassList'), false);
      return mainPoint;
    } catch (e) {
      return {};
    }
  }

  transform(mainPoint, scaleAccordingToData = true) {
    let [maxX, minX, maxY, minY] = [mainPoint.x, mainPoint.x, mainPoint.y, mainPoint.y];
    let count = 0;
    mainPoint.matches = shuffle(mainPoint.matches);
    mainPoint.matches = mainPoint.matches.filter(match => match.isConnected || (count++ < MAX_MATCHES));
    mainPoint.matches.forEach(match => {
      [maxX, minX, maxY, minY] = [Math.max(maxX, match.x), Math.min(minX, match.x), Math.max(maxY, match.y), Math.min(minY, match.y)];
    });
    if (!scaleAccordingToData) [maxX, minX, maxY, minY] = [MAX_VALUE, MIN_VALUE, MAX_VALUE, MIN_VALUE];
    const normalize = (value, minValue, maxValue) => (maxValue - minValue && (value - minValue) / (maxValue - minValue)) * (MAX_RANGE - MIN_RANGE) + MIN_RANGE;
    const scalePoint = point => [point.x, point.y] = [normalize(point.x, minX, maxX), normalize(point.y, minY, maxY)];
    scalePoint(mainPoint);
    mainPoint.matches.forEach(scalePoint);
    return mainPoint;
  }
})();
export default CompassService;
