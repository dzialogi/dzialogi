import React from 'react';
import { Dimensions, View } from 'react-native';

const vh = (Dimensions.get('window').height) / 100;
const vw = Dimensions.get('window').width / 100;

export default ({ p1, p2 }) => {
  const x = (p1.x - p2.x) * vw;
  const y = (p1.y - p2.y) * (vh);
  const width = Math.sqrt(x * x + y * y);
  const left = -x / 2;
  const top = -y / 2;
  const rotate = 90 - Math.atan(x / y) * 180 / Math.PI;
  return <View style={ {
    position: 'absolute',
    transform: [{ rotate: rotate + 'deg' },/* { translateX: (x < 0 ? -1 : 1) * ((width - Math.abs(x)) / 2) }, */{ translateY: -(y / 2) - (y >= 0 ? -1 : 1) * (2.5) }],
    [y < 0 ? 'top' : 'bottom']: '50%',
    [x < 0 ? 'left' : 'right']: '50%',
    height: 5,
    width: width,
    backgroundColor: 'black',
    zIndex: 0
  } }/>;
};