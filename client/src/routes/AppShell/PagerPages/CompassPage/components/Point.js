import React from 'react';
import { View } from 'react-native';
import Line from './Line';

export default ({ x, y, main = false, matches = [] }) => <View style={{
  position: 'absolute',
  left: x + '%',
  top: y + '%',
  transform: [{ translateX: -10 * (main + 1) / 2 }, { translateY: -10 * (main + 1) / 2 }],
  width: 10 * (main + 1),
  height: 10 * (main + 1),
  backgroundColor: main ? 'blue' : 'red',
  borderRadius: 5 * (main + 1),
  alignItems: 'center',
  justifyContent: 'center',
  zIndex: 10
}}>
  {matches.map((point, key) => <Line p1={{ x, y }} p2={point} key={key}/>)}
</View>