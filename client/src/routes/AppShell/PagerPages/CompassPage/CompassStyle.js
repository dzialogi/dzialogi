export const CompassStyle = {
  mapLinesColor: '#ccc',
  mapMainLinesColor: 'green',
  myLocationColor: 'red',
  locationColor: 'black',
  linesColor: 'white',
  ScrollView: {
    flex: 1,
    flexGrow: 1,
    zIndex: 1,
  },
  MainLineX: {
    position: 'absolute',
    top: '50%',
    left: '0%',
    height: 2,
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.12)'
  },
  MainLineY: {
    position: 'absolute',
    left: '50%',
    top: '0%',
    height: '100%',
    width: 2,
    backgroundColor: 'rgba(255,255,255,0.12)'
  },
  MainLineXLabelWrapper: {
    width: '100%',
    position: 'absolute',
    top: '50%',
    left: '0%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    transform: [{
      translateY: -20
    }],
    paddingHorizontal: 5
  },
  MainLineXLabel: {
    color: '#fff'
  },
  MainLineYLabelWrapper: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    left: '0%',
    top: '0%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 5
  },
  MainLineYLabel: {
    color: '#fff'
  },
  Circle: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    borderRadius: 300,
    borderWidth: 36,
    borderColor: '#fff',
    Small: {
      transform: [
        {
          translateX: -35
        },
        {
          translateY: -35
        }
      ],
      width: 0,
      height: 0,
      opacity: 0.12
    },
    Medium: {
      transform: [
        {
          translateX: -86
        },
        {
          translateY: -86
        }
      ],
      width: 174,
      height: 174,
      opacity: 0.06
    },
    Big: {
      transform: [
        {
          translateX: -137
        },
        {
          translateY: -137
        }
      ],
      width: 276,
      height: 276,
      opacity: 0.03
    },
    Dashed: {
      transform: [
        {
          translateX: -188
        },
        {
          translateY: -188
        }
      ],
      width: 378,
      height: 378,
      opacity: 0.03,
      borderStyle: 'dotted'
    }
  },
  SelectionStyle: {
    position: 'absolute',
    left: 0,
    bottom: 0
  }
};