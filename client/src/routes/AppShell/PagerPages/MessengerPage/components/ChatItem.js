import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { StackActions } from 'react-navigation';
import NavigationService from '../../../../../services/NavigationService';
import { getUserAvatar, getUserName } from '../../../../../utils/user';
import { useGlobalState } from '../../../../../State';
import DzialogiText from '../../../../../components/DzialogiText';
import { getPartner } from '../../../../../utils/messenger';

const styles = {
  chatRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 80
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 30,
    marginRight: 10,
    marginLeft: 20
  },
  lastMessage: {
    color: '#afafaf',
  },
  name: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  partnerInfo: {
    flex: 1,
    overflow: 'hidden'
  }
};

const ChatItem = ({ chat, lastMessage, seen }) => {
  const [{ user },] = useGlobalState();

  let partnerUser = getPartner(chat, user);
  let partnerName = getUserName(partnerUser);

  let partnerAvatar = getUserAvatar(partnerUser);

  const onChatClick = chat => {

    const pushAction = StackActions.push({
      routeName: 'Messenger',
      params: {
        chat,
      },
    });

    NavigationService.navigation.dispatch(pushAction);
  };

  const additionalStyles = seen ? {} : { color: '#111', fontWeight: 'bold' };

  return (
    <TouchableOpacity style={styles.chatRow} onPress={() => onChatClick(chat)}>
      <Image
        style={styles.avatar}
        source={{ uri: partnerAvatar }}
      />
      <View style={styles.partnerInfo}>
        <DzialogiText style={styles.name}>{partnerName}</DzialogiText>
        {!!lastMessage &&
        <DzialogiText style={[styles.lastMessage, additionalStyles]} numberOfLines={2}>
          <DzialogiText>{lastMessage}</DzialogiText>
        </DzialogiText>
        }
      </View>
    </TouchableOpacity>
  );
};

export default ChatItem;
