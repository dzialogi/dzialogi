import React from 'react';
import ChatItem from './ChatItem';
import { useGlobalState } from '../../../../../State';

const ChatList = ({ chats }) => {
  const [{ user },] = useGlobalState();

  const getChatItems = () => {
    let res = [];
    if (chats) {
      chats.forEach(chat => chat.lastMessage && (chat.lastMessage.date = new Date(chat.lastMessage.date)));
      chats.sort((a, b) => !(b.lastMessage && b.lastMessage.date) ? 1
        : !(a.lastMessage && a.lastMessage.date) ? -1
          : b.lastMessage.date - a.lastMessage.date);
      for (let chat of chats) {
        const lastMessage = chat.lastMessage;
        let lastMessageText = lastMessage && lastMessage.text || '';
        if (lastMessage && lastMessage.image)
          lastMessageText = 'Photo';

        const seen = lastMessage && (lastMessage.status === 'R' || !lastMessage.user || lastMessage.user._id === user._id);

        res.push(<ChatItem lastMessage={lastMessageText}
                           seen={seen}
                           chat={chat}
                           key={chat._id}/>);
      }
    }

    return res;
  };

  return (
    <>{getChatItems()}</>
  );
};

export default ChatList;
