import React, { useEffect, useState } from 'react';
import { RefreshControl, ScrollView, TextInput, View } from 'react-native';
import ChatList from './components/ChatList';
import { AppShellStyle } from '../../tabBarStyle';
import { globalStateMethods, useChatsState, useGlobalState, useGlobalTabIndex } from '../../../../State';
import { getPartner } from '../../../../utils/messenger';
import { getUserName } from '../../../../utils/user';
import { NoDataPlaceholder } from '../../components/NoDataPlaceholder';
import { NoChatsImg } from '../../../../../assets';
import { subscribeToNotificationChannel, unsubscribeFromNotification } from '../../../../utils/PushNotifications';
import { Notifications } from 'expo';
import { Keys } from '../../routesKeys';

const Style = {
  flex: 1,
  SearchWrapper: {
    padding: 10,
    zIndex: 2,
    backgroundColor: AppShellStyle.backgroundColor
  },
  SearchInput: {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 5,
    paddingLeft: 50,
  },
  ScrollView: {
    flexGrow: 1,
    zIndex: 1,
    padding: 10
  }
};

export default () => {
  const [index,] = useGlobalTabIndex();
  const [{ user },] = useGlobalState();

  const [refreshing, setRefreshing] = useState(false);

  const [chatsData, setState] = useChatsState();

  const [filterTerm, setFilterTerm] = useState('');

  const [filteredChats, setFilteredChats] = useState([]);

  useEffect(() => {
    let filtered = [];
    if (chatsData.chats)
      filtered = chatsData.chats.filter(chat => {
        let userName = getUserName(getPartner(chat, user)).trim();
        return userName.toLowerCase().includes(filterTerm.toLowerCase());
      });
    setFilteredChats(filtered);
  }, [chatsData, filterTerm]);

  useEffect(() => {
    globalStateMethods.fetchChats(setState);
  }, []);

  useEffect(() => {
    const handle = subscribeToNotificationChannel('chat', notification =>
      index.index === Keys.MESSENGER &&
      Notifications.dismissNotificationAsync(notification.notificationId).then());

    return () => {
      unsubscribeFromNotification(handle);
    };
  }, [index]);

  const onRefresh = async () => {
    setRefreshing(true);
    await globalStateMethods.fetchChats(setState);
    setRefreshing(false);
  };

  const onFilter = (term) => {
    setFilterTerm(term.trim());
  };

  return <View style={Style}>
    <View style={Style.SearchWrapper}>
      <TextInput placeholder={'ძებნა...'} style={Style.SearchInput} onChangeText={onFilter}/>
    </View>
    <ScrollView
      contentContainerStyle={Style.ScrollView}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }>
      {chatsData.chats && !filteredChats.length ?
        <NoDataPlaceholder icon={NoChatsImg} text={'აქტიური ძიალოგები არ მოიძებნა'}/>
        :
        <ChatList chats={filteredChats}/>
      }
    </ScrollView>
  </View>;
}
