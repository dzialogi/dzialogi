import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const ChatsService = new (class ChatsService extends AbstractService {
  subject = new WSSubject('chat').subscribe({});

  async getChats() {
    try {
      return await this.subject.emit('getChats');
    } catch (ignored) {
      return [];
    }
  }

  async setAgreement(chatId, agreement) {
    try {
      return await this.subject.emit('setAgreement', { chatId, agreement });
    } catch (ignored) {
      return [];
    }
  }
})();
export default ChatsService;
