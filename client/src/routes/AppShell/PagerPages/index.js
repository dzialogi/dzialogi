export CompassPage from './CompassPage';
export MatchesPage from './MatchesPage';
export MessengerPage from './MessengerPage';
export NewsFeedPage from './NewsFeedPage';
export QuizPage from './QuizPage';
export RequestsPage from './RequestsPage';
