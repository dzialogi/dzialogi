import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const MatchesService = new (class MatchesService extends AbstractService {
  deleteMatch;
  setMatches;
  subject = new WSSubject('matches').subscribe({
    delete: ({ username }) => {
      this.deleteMatch(username);
    },
    set: matches => {
      this.setMatches({ matches });
    }
  });

  async getMatches() {
    try {
      return await this.subject.emit('getMatchingList');
    } catch (e) {
      return [];
    }
  }

  async accept(username) {
    try {
      return await this.subject.emit('accept', username);
    } catch (e) {
      return {};
    }
  }

  async reject(username) {
    try {
      return await this.subject.emit('reject', username);
    } catch (e) {
      return {};
    }
  }
})();
export default MatchesService;
