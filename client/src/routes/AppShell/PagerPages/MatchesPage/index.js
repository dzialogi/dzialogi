import React, { useEffect, useState } from 'react';
import { RefreshControl, ScrollView, View } from 'react-native';
import { MatchRequestItem } from '../../../../components';
import MatchesService from './MatchesService';
import { globalStateMethods, useMatchesState } from '../../../../State';
import SearchComponent from '../../components/SearchComponent';
import { NoDataPlaceholder } from '../../components/NoDataPlaceholder';
import { NoMatchesImg } from '../../../../../assets';
import { withLoading } from '../../../../utils/loader';

const Style = {
  flex: 1,
  ScrollView: {
    flexGrow: 1,
    zIndex: 1,
  }
};

export default () => {
  const [refreshing, setRefreshing] = useState();
  let [{ matches }, setState] = useMatchesState();
  const [filteredData, setFilteredData] = useState([]);
  MatchesService.deleteMatch = username => setState({ matches: matches.filter(match => match.username !== username) });
  MatchesService.setMatches = setState;
  useEffect(() => {
    globalStateMethods.fetchMatches(setState);
  }, []);

  const removePoint = match => {
    matches = matches.filter(listMatch => listMatch.username !== match.username);
    setState({ matches });
  };

  const handleReject = async match => {
    try {
      const res = await withLoading(MatchesService.reject(match.username));
      if (res.status === 'ok')
        removePoint(match);
    } catch (e) {

    }
  };

  const handleAccept = async match => {
    try {
      const res = await withLoading(MatchesService.accept(match.username));
      removePoint(match);
      if (res.status === 'ACCEPTED') {
        globalStateMethods.fetchChats(setState);
      }
    } catch (e) {

    }
  };

  const onRefresh = async () => {
    setRefreshing(true);
    await globalStateMethods.fetchMatches(setState);
    setRefreshing(false);
  };

  return <View style={Style}>
    <SearchComponent data={matches} getKey={request => request.username}
                     onFilter={filteredData => setFilteredData(filteredData)}/>
    <ScrollView
      contentContainerStyle={Style.ScrollView}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }>
      {matches && filteredData && !filteredData.length ?
        <NoDataPlaceholder icon={NoMatchesImg} text={'ჩვენ ჯერ კიდევ ვეძებთ თქვენთვის ძიალოგებს'}/>
        :
        <>
          {!!filteredData && !!filteredData.length && filteredData.map((match, key) =>
            <MatchRequestItem match={match} key={key} onAccept={handleAccept} onReject={handleReject}/>)}
        </>
      }
    </ScrollView>

  </View>;
}
