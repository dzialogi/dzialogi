import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const N_MATCHED_POINTS = 10;

const RequestsService = new (class RequestsService extends AbstractService {
  insertOffer;
  subject = new WSSubject('offers').subscribe({
    insert: data => {
      this.insertOffer(data);
    }
  });

  async getRequests() {
    try {
      return await this.subject.emit('getRequestsList');
    } catch (e) {
      return [];
    }
  }

  async accept(username) {
    try {
      return await this.subject.emit('accept', username);
    } catch (e) {
      return {};
    }
  }

  async reject(username) {
    try {
      return await this.subject.emit('reject', username);
    } catch (e) {
      return {};
    }
  }
})();
export default RequestsService;
