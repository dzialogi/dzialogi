import React, { useEffect, useState } from 'react';
import { RefreshControl, ScrollView, View } from 'react-native';
import { MatchRequestItem } from '../../../../components';
import { MessageCircleIcon } from '../../../../../assets/icons';
import { globalStateMethods, useRequestsState } from '../../../../State';
import RequestsService from './RequestsService';
import SearchComponent from '../../components/SearchComponent';
import { NoDataPlaceholder } from '../../components/NoDataPlaceholder';
import { NoMatchesImg } from '../../../../../assets';
import { withLoading } from '../../../../utils/loader';
import MessengerService from '../../OverlayPages/Messenger/MessengerService';
import NavigationService from '../../../../services/NavigationService';

const Style = {
  flex: 1,
  ScrollView: {
    flexGrow: 1,
    zIndex: 1,
  }
};

export default () => {
  const [refreshing, setRefreshing] = useState();
  let [requestsState, setState] = useRequestsState();
  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    globalStateMethods.fetchRequests(setState).then();
  }, []);

  useEffect(() => {
    RequestsService.insertOffer = offer => {
      setState({ requests: [...requestsState.requests, offer] });
    };
  }, [requestsState]);

  const removePoint = request => {
    requestsState.requests = requestsState.requests.filter(listMatch => listMatch.username !== request.username);
    setState({ requests: requestsState.requests });
  };

  const handleReject = async match => {
    try {
      const res = await withLoading(RequestsService.reject(match.username));
      if (res.status === 'OK')
        removePoint(match);
    } catch (e) {
    }
  };

  const handleAccept = async match => {
    try {
      const res = await withLoading(RequestsService.accept(match.username));
      if (res.status === 'OK') {
        globalStateMethods.fetchChats(setState);
        removePoint(match);
        try {
          const chat = await withLoading(MessengerService.getChatByUsername(match.username));
          NavigationService.navigation.navigate('Messenger', { chat });
        } catch (e) {

        }
      }
    } catch (e) {
    }
  };

  const onRefresh = async () => {
    setRefreshing(true);
    await globalStateMethods.fetchRequests(setState);
    setRefreshing(false);
  };

  return <View style={Style}>
    <SearchComponent data={requestsState.requests} getKey={request => request.username}
                     onFilter={filteredData => setFilteredData(filteredData)}/>
    <ScrollView
      contentContainerStyle={Style.ScrollView}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }>
      {requestsState.requests && filteredData && !filteredData.length ?
        <NoDataPlaceholder icon={NoMatchesImg} text={'ძალიან მალე აუცილებლად შემოგთავაზებენ ძიალოგს'}/>
        :
        <>
          {!!filteredData && !!filteredData.length && filteredData.map((match, key) =>
            <MatchRequestItem match={match} key={key} onAccept={handleAccept} onReject={handleReject}
                              acceptIcon={MessageCircleIcon}/>)}
        </>
      }
    </ScrollView>

  </View>;
}
