import React from 'react';
import { View } from 'react-native';
import { quizGradientColors, quizItemStyle, quizPageStyle } from './style';
import { QuizItem } from './components/QuizItem';
import NavigationService from '../../../../services/NavigationService';
import DzialogiText from '../../../../components/DzialogiText';
import { DailyQuiz, PersonalQuiz, PoliticalQuiz } from '../../../../../assets/icons';

export default () => {

  const goToQuiz = (quizType, quizIcon, gradientColors) => {
    NavigationService.navigation.navigate('Quiz', { quizType, quizIcon, gradientColors });
  };

  return <View style={quizPageStyle}>
    <DzialogiText style={quizPageStyle.Title}>შეარჩიეთ ქვიზი</DzialogiText>

    <View style={quizPageStyle.QuizList}>
      <QuizItem icon={PoliticalQuiz}
                gradientColors={quizGradientColors.political}
                onPress={() => goToQuiz('Political', PoliticalQuiz, quizGradientColors.political)}
                style={quizItemStyle.Odd}
                title={'პოლიტიკური'}/>

      <QuizItem icon={PersonalQuiz}
                gradientColors={quizGradientColors.personal}
                onPress={() => goToQuiz('Personal', PersonalQuiz, quizGradientColors.personal)}
                style={quizItemStyle.Even}
                title={'პიროვნული'}/>

      <QuizItem icon={DailyQuiz}
                gradientColors={quizGradientColors.daily}
                onPress={() => goToQuiz('Daily', DailyQuiz, quizGradientColors.daily)}
                style={{ ...quizItemStyle.Odd, ...quizItemStyle.Last }}
                title={'ყოველდღიური'}/>
    </View>

  </View>;
};
