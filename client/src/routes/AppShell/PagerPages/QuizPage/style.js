export const quizPageStyle = {
  flex: 1,
  paddingTop: 50,
  paddingBottom: 20,
  paddingHorizontal: 40,
  Title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 20,
  },
  QuizList: {
    flex: 1,
    justifyContent: 'space-around',
  }
};

export const quizItemStyle = {
  width: 180,
  height: 140,
  justifyContent: 'flex-end',
  Last: {
    marginBottom: 0
  },
  Even: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  Odd: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  Title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 12,
    marginLeft: 20,
    marginBottom: 20,
  },
  Gradient: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    borderRadius: 10,
  },
  Icon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    height: '70%',
    width: '40%',
    resizeMode: 'contain',
  }
};

export const quizGradientColors = {
  political: ['#576bf2', '#ad8eff'],
  personal: ['#f5515f', '#9f041b'],
  daily: ['#f257a9', '#ff8e8e']
};
