import { TouchableOpacity, View } from 'react-native';
import { quizItemStyle } from '../style';
import React from 'react';
import DzialogiText from '../../../../../components/DzialogiText';
import { QuizBackground } from './QuizBackground';

export const QuizItem = ({ icon, gradientColors, children, onPress, style = {}, title }) =>
  <View style={style}>
    <TouchableOpacity onPress={onPress}>
      <QuizBackground style={quizItemStyle} icon={icon} gradientColors={gradientColors}>
        <DzialogiText style={quizItemStyle.Title}>{title}</DzialogiText>
      </QuizBackground>
    </TouchableOpacity>
  </View>;
