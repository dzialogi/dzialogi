import { Image, View } from 'react-native';
import { quizItemStyle } from '../style';
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { ShutterIcon } from '../../../../../../assets/icons';

export const QuizBackground = ({ icon = ShutterIcon, gradientColors = ['#91f551', '#048c9f'], children, style }) =>
  <View style={style}>
    <LinearGradient style={quizItemStyle.Gradient}
                    colors={gradientColors}/>
    <Image source={icon} style={quizItemStyle.Icon}/>
    {children}
  </View>;
