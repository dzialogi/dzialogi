import React from 'react';
import { View } from 'react-native';
import { useGlobalState } from '../../../../State';
import ProfileComponent from '../../OverlayPages/Profile/components/ProfileComponent';


export default () => {
  const [{ user },] = useGlobalState();
  // const username = navigation.getParam('username', user.username);
  if (!user) {
    return <View/>;
  }
  return (<ProfileComponent profileUser={user} isOwnProfile={true}/>);
};
