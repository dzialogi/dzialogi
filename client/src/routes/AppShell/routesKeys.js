export const Keys = {
  NEWS_FEED: 0,
  COMPASS: 1,
  QUIZ: 2,
  MATCHES: 3,
  REQUESTS: 4,
  MESSENGER: 5
};