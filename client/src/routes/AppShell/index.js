import * as React from 'react';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { Messenger, Profile, Quiz } from './OverlayPages';
import AppShell from './AppShell';
import LogOutItem from './components/DrawerItems/LogOutItem';
import ContentComponent from './components/DrawerItems/ContentComponent';
import EditProfile from './OverlayPages/Profile/components/EditProfile';


export default createStackNavigator(
  {
    AppShell: createDrawerNavigator({
        Home: AppShell,
        logout: LogOutItem,
      },
      {
        initialRouteName: 'Home',
        hideStatusBar: false,
        defaultNavigationOptions: {
          header: null
        },
        contentComponent: ContentComponent
      }),
    Messenger,
    Quiz,
    Profile,
    EditProfile
  },
  {
    initialRouteName: 'AppShell',
    defaultNavigationOptions: {
      header: null
    }
  }
);
