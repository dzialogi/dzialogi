const tabBarStyle = {
  backgroundColor: 'white',
  color: 'blue',
  indicatorStyle: {
    backgroundColor: 'blue',
    top: 0
  },
  renderLabel: {
    height: 20,
    width: 20
  },
  iconWrapper: {},
  notificationIndicator: {
    position: 'absolute',
    top: -2,
    right: -2,
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: 'red'
  }
};


export const AppShellStyle = {
  flex: 1,
  backgroundColor: '#f5f5f5'
}; // f3f6ff
export default tabBarStyle;
