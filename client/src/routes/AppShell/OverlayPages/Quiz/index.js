import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import QuizQuestion from './components/QuizQuestion';
import QuizService from './QuizService';
import { createSwitchNavigator } from 'react-navigation';
import QuizDone from './components/QuizDone';
import QuizNone from './components/QuizNone';
import { globalStateMethods, useGlobalState, useMainPointState, useMatchesState } from '../../../../State';
import { withLoading } from '../../../../utils/loader';
import Icon from '../../../../components/Icon';
import NavigationService from '../../../../services/NavigationService';
import { GlobalStyle } from '../../GlobalStyle';
import { quizGradientColors } from '../../PagerPages/QuizPage/style';
import { PoliticalQuiz } from '../../../../../assets/icons';
import NextQuiz from './components/NextQuiz';

const Quiz = ({ navigation }) => {
  const [globalState, setGlobalState] = useGlobalState();
  const [quizQuestion, setQuizQuestion] = useState(undefined);
  let [questionsQueue, setQuestionsQueue] = useState([]);
  const [, setMainPoint] = useMainPointState();
  const [, setMatches] = useMatchesState();

  useEffect(() => {
    getStartQuestion().then();
  }, []);

  const quizType = navigation.getParam('quizType', 'Required');
  const quizIcon = navigation.getParam('quizIcon', PoliticalQuiz);
  const gradientColors = navigation.getParam('gradientColors', quizGradientColors.political);

  const getStartQuestion = async () => {
    try {
      const firstQuestion = await withLoading(QuizService.getFirstQuestion(quizType));
      if (!firstQuestion.question) {
        setQuizQuestion(null);
      } else {
        setQuizQuestion(firstQuestion);
      }
    } catch (e) {
    }
  };

  const isRegistering = () => {
    return ['N', 'P'].includes(globalState.user.userStatus);
  };

  const onAnswer = async answers => {
    if (!answers.length) return alert('გთხოვთ, გასცეთ პასუხი კითხვას');
    const nextQuestions = await QuizService.sendAnswer(answers);
    questionsQueue = nextQuestions.concat(questionsQueue);
    const nextQuestion = questionsQueue.splice(0, 1)[0];
    setQuestionsQueue(questionsQueue);
    globalStateMethods.fetchCompass(setMainPoint);
    globalStateMethods.fetchMatches(setMatches);
    if (!nextQuestion) {
      if (!isRegistering() || quizType === 'Personal') {
        navigation.navigate('QuizDone');
      } else {
        navigation.navigate('NextQuiz');
      }
    } else {
      setQuizQuestion(nextQuestion);
    }
  };

  const showBackButton = () => {
    return !['N', 'P'].includes(globalState.user.userStatus);
  };

  return <View style={{ flex: 1 }}>
    {showBackButton() && <TouchableOpacity style={GlobalStyle.BackArrow}
                                           onPress={() => NavigationService.navigation.goBack(null)}>
      <Icon name="arrow-back" size={32} color="#555"/>
    </TouchableOpacity>}
    {quizQuestion !== undefined && (quizQuestion !== null ?
      <QuizQuestion question={quizQuestion} quizIcon={quizIcon} gradientColors={gradientColors}
                    onAnswer={answer => withLoading(() => onAnswer(answer))}/>
      :
      <QuizNone/>)
    }
  </View>;
};

export default createSwitchNavigator(
  {
    Quiz,
    QuizDone,
    NextQuiz,
    QuizNone
  },
  {
    initialRouteName: 'Quiz'
  }
);
