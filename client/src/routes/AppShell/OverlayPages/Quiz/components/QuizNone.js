import React from 'react';
import { Image, View } from 'react-native';
import { AllDone } from '../../../../../../assets';
import { DzialogiText } from '../../../../../../src/components';

const Style = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  Logo: {
    top: -10
  },
  Text: {
    fontSize: 30,
    textAlign: 'center',
    color: '#9b9b9b',
    top: -10
  }
};

const QuizNone = () => {
  return (
    <View style={Style}>
      <Image source={AllDone} style={Style.Logo}/>
      <DzialogiText style={Style.Text}>ქუიზები არ არის!</DzialogiText>
    </View>
  );
};

export default QuizNone;