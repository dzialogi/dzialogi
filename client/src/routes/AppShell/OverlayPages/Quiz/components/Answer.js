import React from 'react';
import { CheckBox } from 'react-native-elements';

const Answer = ({ answer, checked, onSelect }) => {
  return (
    <CheckBox onPress={() => onSelect(answer)} title={answer.answer} checked={checked}/>
  );
};

export default Answer;