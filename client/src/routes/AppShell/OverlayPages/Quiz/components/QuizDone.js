import React from 'react';
import { Dimensions, Image, View } from 'react-native';
import { Button } from 'react-native-elements';
import { DoneQuizPage } from '../../../../../../assets';
import DzialogiText from '../../../../../components/DzialogiText';
import { withLoading } from '../../../../../utils/loader';
import { globalStateMethods, useMainPointState, useMatchesState } from '../../../../../State';

const size = Dimensions.get('window').width;

const QuizDoneStyle = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  Text: {
    flex: 0.5,
    fontSize: 30,
    textAlign: 'center',
    color: '#000c4b'
  },
  Button: {
    bottom: 150,
    width: 200,
    zIndex: 1
  },
  BackgroundImage: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: size,
    height: size,
    zIndex: 0
  }
};

const QuizDone = ({ navigation }) => {
  const [, setMainPoint] = useMainPointState();
  const [, setMatches] = useMatchesState();
  const onAppShellNavigation = () => {
    withLoading(() => globalStateMethods.fetchCompass(setMainPoint)).then();
    withLoading(() => globalStateMethods.fetchMatches(setMatches)).then();
    navigation.navigate('AppShell');
  };
  return (
    <View style={QuizDoneStyle}>
      <Image source={DoneQuizPage} style={QuizDoneStyle.BackgroundImage}/>
      <DzialogiText style={QuizDoneStyle.Text}>თქვენ წარმატებით დაასრულეთ ქვიზი</DzialogiText>
      <Button onPress={onAppShellNavigation} title="კომპასი" buttonStyle={QuizDoneStyle.Button}/>
    </View>
  );
};

export default QuizDone;
