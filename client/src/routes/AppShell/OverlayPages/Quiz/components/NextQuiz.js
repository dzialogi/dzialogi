import React from 'react';
import { Dimensions, Image, View } from 'react-native';
import { Button } from 'react-native-elements';
import { DoneQuizPage } from '../../../../../../assets';
import DzialogiText from '../../../../../components/DzialogiText';
import { PersonalQuiz } from '../../../../../../assets/icons';
import { quizGradientColors } from '../../../PagerPages/QuizPage/style';

const size = Dimensions.get('window').width;

const Style = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  Text: {
    flex: 0.5,
    marginHorizontal: 10,
    fontSize: 18,
    textAlign: 'center',
    color: '#000c4b'
  },
  Button: {
    bottom: 150,
    width: 200,
    zIndex: 1
  },
  BackgroundImage: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: size,
    height: size,
    zIndex: 0
  }
};

const NextQuiz = ({ navigation }) => {
  const onNextQuiz = () => {
    navigation.navigate('Quiz', {
      quizType: 'Personal',
      quizIcon: PersonalQuiz,
      gradientColors: quizGradientColors.personal
    });
  };
  return (
    <View style={Style}>
      <Image source={DoneQuizPage} style={Style.BackgroundImage}/>
      <DzialogiText style={Style.Text}>თქვენი პოლიტიკური კოორდინატები დადგენილია! გთხოვთ შეავსოთ ქუიზი თქვენი
        პოერსონალური ინტერესების დასადგენად</DzialogiText>
      <Button onPress={onNextQuiz} title="დაწყება" buttonStyle={Style.Button}/>
    </View>
  );
};

export default NextQuiz;
