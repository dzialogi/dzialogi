import React, { useEffect, useState } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import Answer from './Answer';
import { useGlobalPadding } from '../../../../../State';
import DzialogiText from '../../../../../components/DzialogiText';
import { QuizBackground } from '../../../PagerPages/QuizPage/components/QuizBackground';
import { LinearGradient } from 'expo-linear-gradient';
import { getWindowHeight } from '../../../../../utils/status-bar-height';

const QuizStyle = {
  flex: 1,
  paddingTop: 60,
  Question: {
    marginHorizontal: 20,
    marginBottom: 20,
    opacity: 0.8,
    Text: {
      textAlign: 'center',
      fontSize: 20,
      lineHeight: 26,
      paddingHorizontal: 20,
      paddingVertical: 30,
      color: 'white'
    }
  },
  Footer: {
    Main: {
      position: 'absolute',
      width: '100%',
      bottom: 0,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
    },
    Button: {
      paddingTop: 20,
      paddingBottom: 20,
      flex: 1,
      color: 'white',
      fontSize: 16,
      fontWeight: 'bold'
    }
  }
};

const QuizQuestion = ({ question, quizIcon, gradientColors, onAnswer }) => {
  const [paddingTop,] = useGlobalPadding();
  let [checked, setChecked] = useState([]);
  useEffect(() => setChecked([]), [question]);

  const onSelect = answer => {
    answer.checked = !answer.checked;
    if (answer.checked)
      checked.push(answer._id);
    else
      checked = checked.filter(item => item !== answer._id);
    setChecked(checked);
  };

  const windowHeight = getWindowHeight(paddingTop);

  return (
    <View style={{ height: windowHeight }}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          zIndex: 1,
        }}>
        <View style={[QuizStyle, { paddingTop: paddingTop + QuizStyle.paddingTop, paddingBottom: 100 }]}>
          <QuizBackground style={QuizStyle.Question} icon={quizIcon} gradientColors={gradientColors}>
            <DzialogiText style={QuizStyle.Question.Text}>{question.question}</DzialogiText>
          </QuizBackground>
          <View style={{ paddingHorizontal: 10 }}>
            {question.answers.map(answer => (
              <Answer answer={answer} onSelect={onSelect}
                      checked={answer.checked}
                      key={answer._id}/>
            ))}
          </View>
        </View>
      </ScrollView>
      <LinearGradient style={QuizStyle.Footer.Main} colors={gradientColors}>
        <TouchableOpacity onPress={() => onAnswer(checked)} style={{ flex: 1, alignItems: 'center' }}>
          <Text style={QuizStyle.Footer.Button}>
            შემდეგი
          </Text>
        </TouchableOpacity>
      </LinearGradient>
    </View>
  );
};

export default QuizQuestion;