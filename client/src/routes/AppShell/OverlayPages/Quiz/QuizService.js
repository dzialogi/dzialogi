import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const QuizService = new (class QuizService extends AbstractService {
  subject = new WSSubject('quiz').subscribe({});

  async getFirstQuestion(quizType) {
    try {
      return await this.subject.emit('getFirstQuestion', quizType);
    } catch (e) {
      return {};
    }
  }

  async sendAnswer(answerId) {
    try {
      return await this.subject.emit('sendAnswer', answerId);
    } catch (e) {
      return {};
    }
  }
})();
export default QuizService;
