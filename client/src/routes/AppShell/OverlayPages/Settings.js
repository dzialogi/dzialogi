import React from 'react';
import { View } from 'react-native';
import DzialogiText from '../../../components/DzialogiText';

export default class extends React.PureComponent {
  static navigationOptions = {
    title: 'Settings'
  };

  render() {
    return <View>
      <DzialogiText>Settings Page</DzialogiText>
    </View>;
  }
}
