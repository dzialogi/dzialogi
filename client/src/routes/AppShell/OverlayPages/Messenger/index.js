import React, { useEffect, useState } from 'react';
import { Image, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useGlobalNotifications, useGlobalPadding, useGlobalState } from '../../../../State';
import { getUserAvatar, getUserName } from '../../../../utils/user';
import { GiftedChat, Send } from 'react-native-gifted-chat';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import DzialogiText from '../../../../components/DzialogiText';
import KeyboardSpacer from '../../../../components/KeyboardSpacer';
import { bothAgreed, getAgreement, getPartner, setAgreement } from '../../../../utils/messenger';
import MessengerService from './MessengerService';
import { withLoading } from '../../../../utils/loader';
import NavigationService from '../../../../services/NavigationService';
import { Icon } from '../../../../components';
import ChatsService from '../../PagerPages/MessengerPage/ChatsService';
import { subscribeToNotificationChannel, unsubscribeFromNotification } from '../../../../utils/PushNotifications';
import { Notifications } from 'expo';
import { Keys } from '../../routesKeys';


const styles = {
  chat: {
    backgroundColor: '#1fb6ff',
    flex: 1
  },
  header: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#1fb6ff',
    actions: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      Icon: {
        marginLeft: 20
      }
    }
  },
  username: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff'
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 20,
    marginRight: 20,
  },
  chatContent: {
    backgroundColor: '#ffffff',
    flex: 1
  },
  mapView: {
    width: 150,
    height: 100,
    borderRadius: 13,
    margin: 3,
  },
  sendButton: {
    color: 'deepskyblue',
  },
  sendButtonWrapper: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  }
};

function transformMessage(msg) {
  let res = { ...msg, user: { ...msg.user } };

  res.createdAt = msg.date;
  res.user.name = getUserName(msg.user);
  res.user.avatar = getUserAvatar(msg.user);

  return res;
}

function transformMessages(messages) {
  return messages.filter(msg => !!msg.user).map(msg => transformMessage(msg)).reverse();
}

export default ({ navigation: { state: { params: { chat } } } }) => {
  const [paddingTop,] = useGlobalPadding();
  const [{ user }, setGlobalState] = useGlobalState();

  const [messages, setMessages] = useState([]);

  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const [noMoreMessages, setNoMoreMessages] = useState(false);

  const [refreshCount, refresh] = useState(0);

  const [notifications, setNotifications] = useGlobalNotifications();

  const messagesToLoad = 20;

  let partner = getPartner(chat, user);

  const fetchMessages = async () => {
    let messages = await withLoading(() => MessengerService.getMessages(chat._id));
    messages = transformMessages(messages);
    setMessages(messages);
  };

  const messageAdded = message => {
    setMessages([transformMessage(message)].concat(messages));
  };

  useEffect(() => {
    MessengerService.setDynamic(chat._id + '_' + partner._id);

    fetchMessages().then();

    const handle = subscribeToNotificationChannel('chat', notification =>
      notification.data.chatId === chat._id
      && Notifications.dismissNotificationAsync(notification.notificationId).then());
    if (!notifications[Keys.MESSENGER]) notifications[Keys.MESSENGER] = [];
    notifications[Keys.MESSENGER] =
      notifications[Keys.MESSENGER].filter(
        notification => (notification.data && notification.data.chatId) !== chat._id
      );
    setNotifications(notifications);

    return () => {
      MessengerService.unsubscribeDynamic();
      unsubscribeFromNotification(handle);
    };
  }, []);

  useEffect(() => {
    MessengerService.messageAdded = messageAdded;
  }, [messages]);

  useEffect(() => {
    MessengerService.chatAgreeUpdated = (agreement1, agreement2) => {
      chat.agreement1 = agreement1;
      chat.agreement2 = agreement2;
      refresh(refreshCount + 1);
    };
  }, [refreshCount]);

  const loadMore = async () => {
    if (isLoadingMore || noMoreMessages) return;

    setIsLoadingMore(true);
    let moreMessages = await MessengerService.getMessages(chat._id, messages.length, messagesToLoad);
    moreMessages = transformMessages(moreMessages);
    setIsLoadingMore(false);

    if (!moreMessages.length) {
      setNoMoreMessages(true);
      return;
    }

    setMessages([...messages, ...moreMessages]);
  };

  const renderCustomView = (props) => {
    if (props.currentMessage.location) {
      return (
        <View style={props.containerStyle}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={[styles.mapView]}
            region={{
              latitude: props.currentMessage.location.latitude,
              longitude: props.currentMessage.location.longitude,
              latitudeDelta: 0.1,
              longitudeDelta: 0.1,
            }}
            scrollEnabled={false}
            zoomEnabled={false}
          >
            <MapView.Marker
              coordinate={{
                latitude: props.currentMessage.location.latitude,
                longitude: props.currentMessage.location.longitude
              }}
            />
          </MapView>
        </View>
      );
    }
    return null;
  };

  const sendMessage = async message => {
    message = await MessengerService.addMessage(chat._id, message, partner._id);
    messageAdded(message);
  };

  const onSend = (newMessages = []) => {
    sendMessage(newMessages[0]).then();
  };


  const handleAddPicture = async () => {
    let imageRes = await ImagePicker.launchImageLibraryAsync({ base64: true, quality: 0.1 });

    if (imageRes.cancelled) return;

    let message = { user, image: 'data:image/jpeg;base64,' + imageRes.base64, createdAt: new Date() };

    sendMessage(message).then();
  };

  const navigateToPartner = () => {
    NavigationService.navigation.navigate('Profile', { username: partner.username });
  };

  const onAgree = () => {
    if (getAgreement(chat, user)) return;

    setAgreement(chat, user, true);

    if (bothAgreed(chat)) {
      alert('გილოცავთ! თქვენ ძმები ხართ!');
    } else {
      alert('დაძმობილების თხოვნა გაგზავნილია!');
    }

    ChatsService.setAgreement(chat._id, true).then();

    refresh(refreshCount + 1);
  };

  return <View style={{ ...styles.chat, paddingTop }}>
    <View style={styles.header}>
      <TouchableOpacity style={{ marginRight: 10 }} onPress={() => NavigationService.navigation.goBack(null)}>
        <Icon name="arrow-back" size={32} color="white"/>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigateToPartner} style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Image
          style={styles.avatar}
          source={{ uri: getUserAvatar(partner) }}
        />
        <DzialogiText style={styles.username}>
          {getUserName(partner)}
        </DzialogiText>
      </TouchableOpacity>
      <View style={styles.header.actions}>
        <TouchableOpacity style={styles.header.actions.Icon} onPress={onAgree}>
          {bothAgreed(chat) &&
          <FontAwesome name="handshake-o" size={24} style={{ color: '#ffbf00' }}/>
          }
          {!bothAgreed(chat) &&
          <Icon name="hand" size={24} color={getAgreement(chat, user) ? '#ffbf00' : '#ddd'}/>
          }
        </TouchableOpacity>
        <TouchableOpacity style={styles.header.actions.Icon} onPress={handleAddPicture}>
          <MaterialIcons
            name="add-a-photo"
            size={24}
            color={'white'}
          />
        </TouchableOpacity>
      </View>
    </View>
    <View style={styles.chatContent}>
      {messages.length === 0 && (
        <View style={[
          StyleSheet.absoluteFill,
          {
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            bottom: 50
          }]}>
          <Image
            source={{ uri: 'https://i.stack.imgur.com/qLdPt.png' }}
            style={{
              ...StyleSheet.absoluteFillObject,
              resizeMode: 'contain'
            }}
          />
        </View>
      )}

      <GiftedChat
        loadEarlier={!noMoreMessages}
        isLoadingEarlier={isLoadingMore}
        listViewProps={{
          onEndReached: loadMore,
          onEndReachedThreshold: 0.5
        }}
        alignTop={false}
        isAnimated={true}
        scrollToBottom={true}
        scrollToBottomComponent={() => (
          <Icon name="arrow-down" size={15} color="#555"/>
        )}
        initialText={''}
        placeholder={'აკრიფეთ ტექსტი...'}
        messages={messages}
        onSend={(messages) => onSend(messages)}
        renderCustomView={renderCustomView}
        user={{ _id: user._id, name: getUserName(user), avatar: getUserAvatar(user) }}
        onPressAvatar={navigateToPartner}
        parsePatterns={() => [
          {
            pattern: /#(\w+)/,
            style: { textDecorationLine: 'underline', color: 'lightgreen' },
            onPress: props => alert(`press on ${props}`),
          },
        ]}
        renderSend={props => <Send {...props}><View style={styles.sendButtonWrapper}><DzialogiText
          style={styles.sendButton}>გაგზავნა</DzialogiText></View></Send>}
      />
    </View>
    {Platform.OS === 'android' ? <KeyboardSpacer/> : null}
  </View>;
};

