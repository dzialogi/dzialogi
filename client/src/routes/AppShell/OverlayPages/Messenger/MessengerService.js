import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const MessengerService = new (class MessengerService extends AbstractService {
  subject = new WSSubject('messenger').subscribe({});

  async getMessages(chatId, from = 0, count = 20) {
    try {
      return await this.subject.emit('getMessages', { from, count, chatId });
    } catch (ignored) {
      return [];
    }
  }

  async addMessage(chatId, message, partnerId) {
    try {
      return await this.subject.emit('addMessage', { chatId, message, partnerId });
    } catch (ignored) {
      return [];
    }
  }

  async getChat(chatId) {
    try {
      return await this.subject.emit('getChat', chatId);
    } catch (ignored) {
      return {};
    }
  }

  async getChatByUsername(username) {
    try {
      return await this.subject.emit('getChatByUsername', username);
    } catch (ignored) {
      return {};
    }
  }

  dynamicSubject;
  messageAdded;
  chatAgreeUpdated;

  async setDynamic(dynamicName) {
    await this.unsubscribeDynamic();
    this.dynamicSubject = new WSSubject('messenger', dynamicName).subscribe({
      messageAdded: post => {
        if (this.messageAdded)
          this.messageAdded(post);
      },
      chatAgreeUpdated: ({ agreement1, agreement2 }) => {
        if (this.chatAgreeUpdated)
          this.chatAgreeUpdated(agreement1, agreement2);
      }
    });
  }

  async unsubscribeDynamic() {
    if (this.dynamicSubject) {
      await this.dynamicSubject.unsubscribe();
      this.dynamicSubject = null;
    }
  }
})();
export default MessengerService;
