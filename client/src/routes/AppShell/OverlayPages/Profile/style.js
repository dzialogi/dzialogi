const horizontalPadding = 20;

export const mainStyles = {
  wrapper: {
    paddingTop: 80
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: horizontalPadding,
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: '#e5e5e5',
    marginTop: 20,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 20,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  editBtnWrapper: {
    flex: 1,
    alignItems: 'flex-end',
  },
  editBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    borderRadius: 25,
    marginRight: 5,
  },
  feedTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    marginVertical: 30,
    marginHorizontal: horizontalPadding,
  }
};

export const newPostStyles = {
  body: {
    marginHorizontal: horizontalPadding,
    borderRadius: 5,
    marginBottom: 10,
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
  },
  input: {},
  postBtnBlock: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    btn: {
      backgroundColor: '#2289dc',
      borderRadius: 5,
      paddingHorizontal: 10,
      flexDirection: 'row',
      justifyContent: 'center',
      paddingVertical: 5,
      text: {
        color: 'white',
        fontSize: 12
      }
    }
  }
};

export const postStyles = {
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: horizontalPadding
  },
  body: {
    flex: 1,
    borderRadius: 5,
    marginBottom: 10,
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
    selected: {
      elevation: 5,
    }
  },
  postTime: {
    fontSize: 12,
    color: '#b7b7b7',
    marginRight: 10,
  },
  divider: {
    backgroundColor: '#d5d5d5',
    height: 1,
    marginVertical: 15
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    button: {
      flexDirection: 'row',
      alignItems: 'center',

      marginRight: 10,
      icon: {
        marginRight: 5
      },
      text: {
        fontSize: 12
      }
    }
  },
  deleteBtn: {
    marginLeft: 10,
    backgroundColor: '#f5364e',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    borderRadius: 25,
    marginRight: 5,
    elevation: 3,
  },
  quizQuestion: {
    fontWeight: 'bold',
    lineHeight: 18
  },
  quizAnswer: {}
};

export const editProfileStyles = {
    wrapper: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F3F6FF',
    },
    inputStyle: {
      backgroundColor: 'white',
      width: 300,
      marginBottom: 20,
      padding: 10,
      borderRadius: 3
    },
    avatarBlock: {
      alignItems: 'center',
      marginBottom: 20,
      avatar: {
        width: 120,
        height: 120,
        borderRadius: 60,
      },
      btnOverlay: {
        width: 120,
        height: 120,
        borderRadius: 60,
        position: 'absolute',
        top: 0,
        left: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        alignItems: 'center',
        justifyContent: 'center',
        changeBtn: {
          color: 'black',
          fontSize: 12,
          borderWidth: 1,
          borderColor: '#ccc',
          borderRadius: 5,
          width: 60,
          textAlign: 'center',
          backgroundColor: 'white'
        }
      }
    }
  }
;
