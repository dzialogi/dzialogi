import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import ProfileComponent from './components/ProfileComponent';
import UsersService from '../../../../services/UsersService';
import NavigationService from '../../../../services/NavigationService';
import { Icon } from '../../../../components';
import { GlobalStyle } from '../../GlobalStyle';

const Profile = ({ navigation }) => {
  const [profileUser, setProfileUser] = useState(null);
  useEffect(() => {
    const username = navigation.getParam('username');
    UsersService.getUserByUsername(username).then(user => {
      setProfileUser(user);
    });
  }, []);

  if (!profileUser) return <View/>;

  return (
    <View>
      <TouchableOpacity style={GlobalStyle.BackArrow}
                        onPress={() => NavigationService.navigation.goBack(null)}>
        <Icon name="arrow-back" size={32} color="#555"/>
      </TouchableOpacity>
      <ProfileComponent profileUser={profileUser} isOwnProfile={false}/>
    </View>
  );
};

export default Profile;
