import React, { useEffect, useState } from 'react';
import { Image, KeyboardAvoidingView, TextInput, TouchableOpacity, View } from 'react-native';
import { editProfileStyles } from '../style';
import loginStyle from '../../../../Auth/Login/style';
import { Button } from 'react-native-elements';
import { useGlobalState } from '../../../../../State';
import UsersService from '../../../../../services/UsersService';
import NavigationService from '../../../../../services/NavigationService';
import { withLoading } from '../../../../../utils/loader';
import DzialogiText from '../../../../../components/DzialogiText';
import * as ImagePicker from 'expo-image-picker';
import { getUserAvatar } from '../../../../../utils/user';
import { Icon } from '../../../../../components';
import { GlobalStyle } from '../../../GlobalStyle';

const EditProfile = () => {
  const [globalState, setGlobalState] = useGlobalState();

  const [avatar, setAvatar] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  useEffect(() => {
    if (!globalState.user || !globalState.user.userDetails) return;
    let userDetails = globalState.user.userDetails;
    setFirstName(userDetails.firstName);
    setLastName(userDetails.lastName);
    setAvatar(userDetails.avatar);
  }, [globalState]);

  const onAvatarChange = async () => {
    let imageRes = await ImagePicker.launchImageLibraryAsync({ base64: true, quality: 0.1 });

    if (imageRes.cancelled) return;

    let newAvatar = 'data:image/jpeg;base64,' + imageRes.base64;

    setAvatar(newAvatar);

  };

  const onSave = async () => {
    if (password) {
      if (password !== confirmPassword) {
        alert('პაროლები არ ემთხვევა');
        return;
      }
    }

    let user = await withLoading(UsersService.updateUser(globalState.user._id, firstName, lastName, password, confirmPassword, avatar));
    setGlobalState({ user });
    NavigationService.navigation.goBack(null);
  };

  return (
    <KeyboardAvoidingView style={editProfileStyles.wrapper} behavior="padding" enabled>
      <TouchableOpacity style={GlobalStyle.BackArrow}
                        onPress={() => NavigationService.navigation.goBack(null)}>
        <Icon name="arrow-back" size={32} color="#555"/>
      </TouchableOpacity>
      <View>
        <View style={editProfileStyles.avatarBlock}>
          <View>
            <Image
              resizeMode={'cover'}
              source={{ uri: avatar || getUserAvatar(globalState.user) }}
              style={editProfileStyles.avatarBlock.avatar}/>
            <View style={editProfileStyles.avatarBlock.btnOverlay}>
              <TouchableOpacity onPress={onAvatarChange}>
                <DzialogiText style={editProfileStyles.avatarBlock.btnOverlay.changeBtn}>
                  შეცვლა
                </DzialogiText>
              </TouchableOpacity>
            </View>
          </View>

        </View>
        <TextInput style={editProfileStyles.inputStyle}
                   value={firstName}
                   onChangeText={text => setFirstName(text)}
                   placeholder='სახელი'/>
        <TextInput style={editProfileStyles.inputStyle}
                   value={lastName}
                   onChangeText={text => setLastName(text)}
                   placeholder='გვარი'/>
        <TextInput style={editProfileStyles.inputStyle}
                   value={password}
                   onChangeText={text => setPassword(text)}
                   placeholder='პაროლი'
                   secureTextEntry={true}/>
        <TextInput style={editProfileStyles.inputStyle}
                   value={confirmPassword}
                   onChangeText={text => setConfirmPassword(text)}
                   placeholder='გაიმეორეთ პაროლი'
                   secureTextEntry={true}/>
        <View style={loginStyle.buttonsStyle}>
          <Button
            containerStyle={loginStyle.buttonsStyle.buttonStyle}
            title='შენახვა'
            onPress={onSave}
          />
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

export default EditProfile;
