import { Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import React, { useState } from 'react';
import { postStyles } from '../style';
import { Icon } from '../../../../../components';
import { toDateTimeString } from '../../../../../utils/date';
import DzialogiText from '../../../../../components/DzialogiText';


const PostItem = ({ post, onLike, onDelete, isOwnProfile, isLiked }) => {
  const [selected, setSelected] = useState(false);

  return (
    <View style={postStyles.wrapper}>
      <TouchableWithoutFeedback onLongPress={() => isOwnProfile && setSelected(!selected)}>
        <View style={[postStyles.body, selected ? postStyles.body.selected : {}]}>
          {post.postType.type === 'quiz' ? <>
            <View><DzialogiText style={postStyles.quizQuestion}>{post.quizQuestion}</DzialogiText></View>
            <View><DzialogiText style={postStyles.quizAnswer}>{post.quizAnswer}</DzialogiText></View>
          </> : <DzialogiText>{post.text}</DzialogiText>}
          <View style={postStyles.divider}/>
          <View style={postStyles.buttons}>
            <TouchableOpacity style={postStyles.buttons.button} onPress={() => onLike(post)}>
              <Icon style={postStyles.buttons.button.icon}
                    name="thumbs-up" size={20} color={isLiked ? '#2289dc' : '#555'}/>
              <DzialogiText
                style={postStyles.buttons.button.text}>{post.reacts ? post.reacts.length : 0} likes</DzialogiText>
            </TouchableOpacity>
            <Text
              style={postStyles.postTime}>{post.postType.type === 'quiz' ? 'ნაპასუხებია' : ''} {toDateTimeString(new Date(post.date))}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      {isOwnProfile && selected &&
      <TouchableOpacity style={postStyles.deleteBtn}
                        onPress={() => onDelete(post._id)}>
        <Icon name="trash" size={25} color="white"/>
      </TouchableOpacity>
      }
    </View>
  );
};

export default PostItem;
