import React, { useEffect, useState } from 'react';
import { Image, RefreshControl, ScrollView, TouchableOpacity, View } from 'react-native';
import { withLoading } from '../../../../../utils/loader';
import { globalStateMethods, useGlobalState, usePostsState } from '../../../../../State';
import PostsService from '../PostsService';
import { getUserAvatar, getUserName } from '../../../../../utils/user';
import { mainStyles } from '../style';
import DzialogiText from '../../../../../components/DzialogiText';
import NewPost from './NewPost';
import PostsList from './PostsList';
import { SpinnerIcon } from '../../../../../../assets';
import NavigationService from '../../../../../services/NavigationService';
import { AntDesign } from '@expo/vector-icons';

const LoaderStyles = {
  width: '100%',
  marginVertical: 10,
  alignItems: 'center',
  justifyContent: 'center',
  Image: {
    width: 30,
    height: 30,
  }
};

const ProfileComponent = ({ profileUser, isOwnProfile }) => {
    const [{ user }, setGlobalState] = useGlobalState();

    const [refreshing, setRefreshing] = useState(false);
    const [postsState, setPostsState] = usePostsState();

    const [profilePosts, setProfilePosts] = useState(null);

    const [isLoadingMore, setIsLoadingMore] = useState(false);
    const [noMorePosts, setNoMorePosts] = useState(false);

    const postsToLoad = 10;

    const [callBacks, setCallBacks] = useState({});

    const fetchPosts = async () => {
      if (isOwnProfile) {
        return await globalStateMethods.fetchPosts(setPostsState);
      } else {
        let posts = await PostsService.getPosts(profileUser.username);
        setProfilePosts(posts);
      }
    };

    const getPosts = () => {
      return isOwnProfile ? postsState.posts : profilePosts;
    };

    const setPosts = (posts) => {
      if (isOwnProfile)
        setPostsState({ posts });
      else
        setProfilePosts(posts);
    };

    const refreshPosts = () => {
      if (isOwnProfile)
        setPostsState({ posts: [...postsState.posts] });
      else
        setProfilePosts([...profilePosts]);
    };

    useEffect(() => {
      withLoading(() => fetchPosts()).then();
    }, []);

    useEffect(() => {
      PostsService.setDynamic(profileUser._id, callBacks);
      return () => {
        PostsService.unsubscribeDynamic(profileUser._id);
      };
    }, [profileUser]);

    useEffect(() => {
      callBacks.likeAdded = (postId, like) => {
        if (like.user === user._id) return;

        let post = getPosts().find(post => post._id === postId);
        post.reacts.push(like);
        refreshPosts();
      };

      callBacks.likeRemoved = (postId, userId) => {
        if (userId === user._id) return;

        let post = getPosts().find(post => post._id === postId);
        post.reacts = post.reacts.filter(like => like.user !== userId);
        refreshPosts();
      };

      callBacks.postDeleted = postId => {
        const filteredPosts = getPosts().filter(post => post._id !== postId);
        setPosts(filteredPosts);
      };

      callBacks.postAdded = post => {
        setPosts([post, ...getPosts()]);
      };

      setCallBacks(callBacks);
    }, [profilePosts, postsState]);


    if (!profileUser) {
      return <View/>;
    }
    const onRefresh = async () => {
      setRefreshing(true);
      await fetchPosts();
      setRefreshing(false);
      setNoMorePosts(false);
    };

    const onPostAdd = async text => {
      await withLoading(() => PostsService.post(text));
    };

    const onPostDelete = async postId => {
      await withLoading(() => PostsService.delete(postId));

      const filteredPosts = getPosts().filter(post => post._id !== postId);
      setPostsState({ posts: filteredPosts });
    };

    const onPostLike = async post => {
      if (post.reacts.find(like => like.user === user._id)) {
        post.reacts = post.reacts.filter(like => like.user !== user._id);
        PostsService.removeLike(post._id);
      } else {
        let like = await withLoading(() => PostsService.addLike(post._id));
        post.reacts.push(like);
      }
      refreshPosts();
    };

    const username = getUserName(profileUser);
    const avatar = getUserAvatar(profileUser);

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
      const paddingToBottom = 50;
      return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
    };

    const loadMore = async () => {
      if (isLoadingMore || noMorePosts) return;

      setIsLoadingMore(true);
      let morePosts = await PostsService.getPosts(profileUser.username, getPosts().length, postsToLoad);
      setIsLoadingMore(false);

      if (!morePosts.length) {
        setNoMorePosts(true);
        return;
      }

      setPosts([...getPosts(), ...morePosts]);

    };

    const onEditClick = () => {
      NavigationService.navigation.navigate('EditProfile');
    };

    return (
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          zIndex: 1,
        }}

        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            loadMore();
          }
        }}
        scrollEventThrottle={400}

        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }>

        <View style={mainStyles.wrapper}>
          <View style={mainStyles.header}>
            <Image
              style={mainStyles.avatar}
              source={{ uri: avatar }}
            />
            <DzialogiText style={mainStyles.name}>{username}</DzialogiText>
            {isOwnProfile && <View style={mainStyles.editBtnWrapper}>
              <TouchableOpacity style={mainStyles.editBtn} onPress={onEditClick}>
                <AntDesign name={'edit'} size={25} color="#555"/>
              </TouchableOpacity>
            </View>
            }
          </View>
          <View style={mainStyles.divider}/>
          <DzialogiText
            style={mainStyles.feedTitle}>{isOwnProfile ? 'შენი' : username + '-ის'} ფიდი</DzialogiText>
          {isOwnProfile && <NewPost onPostAdd={onPostAdd}/>}
          <PostsList posts={isOwnProfile ? postsState.posts : profilePosts} onDelete={onPostDelete}
                     onLike={onPostLike}
                     isOwnProfile={isOwnProfile}/>
          {isLoadingMore &&
          <View style={LoaderStyles}>
            <Image source={SpinnerIcon} style={LoaderStyles.Image}/>
          </View>
          }
        </View>
      </ScrollView>
    );
  }
;

export default ProfileComponent;
