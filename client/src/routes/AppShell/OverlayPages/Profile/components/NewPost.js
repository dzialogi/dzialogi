import { TextInput, TouchableOpacity, View } from 'react-native';
import React, { useState } from 'react';
import { newPostStyles } from '../style';
import { useGlobalState } from '../../../../../State';
import DzialogiText from '../../../../../components/DzialogiText';

const NewPost = ({ onPostAdd }) => {
  const [{ user },] = useGlobalState();
  const [text, setText] = useState('');

  const onChangeText = (text) => {
    setText(text);
  };

  const onPost = async () => {
    onPostAdd(text);
    setText('');
  };

  return (
    <View style={newPostStyles.body}>
      <TextInput style={newPostStyles.input} placeholder={'მე ვფიქრობ რომ...'} multiline={true}
                 numberOfLines={3}
                 textAlignVertical={'top'}
                 value={text}
                 onChangeText={onChangeText}
      />

      <View style={newPostStyles.postBtnBlock}>
        <TouchableOpacity style={newPostStyles.postBtnBlock.btn} onPress={onPost}>
          <DzialogiText style={newPostStyles.postBtnBlock.btn.text}>გამოქვეყნება</DzialogiText>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default NewPost;
