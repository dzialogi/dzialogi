import { View } from 'react-native';
import React from 'react';
import PostItem from './PostItem';
import { useGlobalState } from '../../../../../State';

const PostsList = ({ posts, isOwnProfile, onLike, onDelete }) => {
  const [{ user }, setGlobalState] = useGlobalState();

  if (!posts)
    return <View/>;

  const isPostLiked = post => {
    return post.reacts.find(like => like.user === user._id);
  };

  return (
    <View>
      {posts && posts.map(post => (
        <PostItem post={post} key={post._id} onLike={onLike} isLiked={isPostLiked(post)} onDelete={onDelete}
                  isOwnProfile={isOwnProfile}/>
      ))}
    </View>
  );
};

export default PostsList;
