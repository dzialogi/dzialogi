import AbstractService from '../../../../services/AbstractService';
import { WSSubject } from '../../../../services/WSSubject';

const PostsService = new (class PostsService extends AbstractService {

  subject = new WSSubject('posts').subscribe();

  async getPosts(username = '', from = 0, count = 10) {
    try {
      return await this.subject.emit('getPosts', { username, from, count });
    } catch (e) {
      return [];
    }
  }

  async post(text) {
    try {
      return await this.subject.emit('post', text);
    } catch (e) {
      return {};
    }
  }

  async delete(id) {
    try {
      return await this.subject.emit('delete', id);
    } catch (e) {
      return {};
    }
  }

  async addLike(posdId) {
    try {
      return await this.subject.emit('addLike', posdId);
    } catch (e) {
      return {};
    }
  }

  async removeLike(posdId) {
    try {
      return await this.subject.emit('removeLike', posdId);
    } catch (e) {
      return {};
    }
  }

  dynamicSubjects = {};
  postAdded;
  postDeleted;
  likeAdded;
  likeRemoved;

  async setDynamic(dynamicName, callBacks) {
    await this.unsubscribeDynamic(dynamicName);
    this.dynamicSubjects[dynamicName] = new WSSubject('posts', dynamicName).subscribe({
      postAdded: post => {
        if (callBacks.postAdded)
          callBacks.postAdded(post);
      },
      postDeleted: postId => {
        if (callBacks.postDeleted)
          callBacks.postDeleted(postId);
      },
      likeAdded: ({ postId, like }) => {
        if (callBacks.likeAdded)
          callBacks.likeAdded(postId, like);
      },
      likeRemoved: ({ postId, userId }) => {
        if (callBacks.likeRemoved)
          callBacks.likeRemoved(postId, userId);
      }
    });
  }

  async unsubscribeDynamic(dynamicName) {
    if (this.dynamicSubjects[dynamicName]) {
      try {
        await this.dynamicSubjects[dynamicName].unsubscribe();
      } catch (e) {
        
      }
      this.dynamicSubjects[dynamicName] = null;
    }
  }

})();
export default PostsService;
