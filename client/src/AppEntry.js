import React, { useState } from 'react';
import AppShellStack from './routes/AppShell';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Auth from './routes/Auth';
import AuthLoadingScreen from './routes/AuthLoadingScreen';
import preparations from './utils/preparations';
import { Loader } from './utils/loader';


const AppNavigator = createSwitchNavigator(
  {
    AuthLoadingScreen,
    AppShell: AppShellStack,
    Login: Auth
  },
  {
    initialRouteName: 'AuthLoadingScreen'
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default () => {
  const [render, setRender] = useState(false);
  preparations.then(setRender);
  return render ? <><AppContainer/><Loader/></> : <Loader alwaysShow={true}/>;
};
