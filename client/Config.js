import Constants from 'expo-constants';

let config = {
  apiUrl: 'https://dzialogi-test-server.herokuapp.com'
};

// For Development
if (__DEV__) {
  const { manifest } = Constants;
  const api = `http://${ manifest.debuggerHost.split(':')[0] }:3000`;
  config.apiUrl = api;
}

export default config;
