export AppIcon from './icon.png';
export DoneQuizPage from './done_quiz_page.png';
export AllDone from './all_done.png';
export LoaderIcon from './loader.gif';
export SpinnerIcon from './spinner.gif';
export NoChatsImg from './mirage_no_comments.png';
export NoMatchesImg from './mirage_waiting.png';
export NoPostsImg from './mirage_list_is_empty.png';