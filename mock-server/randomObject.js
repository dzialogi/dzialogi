const randomLorem = require('random-lorem');
const loremIpsum = require('lorem-ipsum').loremIpsum;
const api = require('./api');
const toExport = {
  omitFields: (apiName, fields) => {
    const res = {};
    Object.keys(api[apiName].result).forEach(name => {
      res[name] = fields === undefined || fields.includes(name) ? 1 : 0;
    });
    return res;
  },
  generate: body => {
    const res = {};
    Object.keys(body).forEach(resultKey => {
      const min = body[resultKey].min || 1;
      const max = body[resultKey].max || 10;
      const n = Math.round(Math.random() * (max - min) + min);
      ({
        string() {
          const count = n;
          const wordSize = [4, 5, 6, 7, 8, 9, 10, 11, 12, 13].sort((a, b) => count % (a + 1) === count % (b + 1) ? a > b ? -1 : 1 : count % (a + 1) < count % (b + 1) ? -1 : 1)[0];
          const emptyArray = new Array(Math.round(count / wordSize)).fill(0);
          const lorem = () => randomLorem({
            length: wordSize
          });
          res[resultKey] = loremIpsum({
            count,
            format: 'plain',
            units: 'words',
            words: emptyArray.map(lorem)
          });
        },
        number() {
          res[resultKey] = n;
        },
        boolean() {
          res[resultKey] = n % 2 === 0;
        },
        image() {
          const width = body[resultKey].width || 400;
          const height = body[resultKey].height || 200;
          res[resultKey] = `http://lorempixel.com/${width}/${height}/`;
        },
        object(type) {
          if (Array.isArray(type)) {
            res[resultKey] = new Array(n).fill(type[0]).map(toExport.generate);
          }
          res[resultKey] = toExport.generate(type);
        }
      })[
        body[resultKey].type ?
          typeof body[resultKey].type === 'object' ?
            'object' :
            body[resultKey].type :
          typeof body[resultKey] === 'object' ?
            'object' :
            body[resultKey]
        ](body[resultKey].type ? body[resultKey].type : body[resultKey]);
    });
    return res;
  }
};

module.exports = toExport;
