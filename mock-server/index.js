const express = require('express');
const api = require('./api');
const generate = require('./randomObject').generate;
const omitFields = require('./randomObject').omitFields;

const app = express();
const PORT = process.env.PORT || 3000;

const handler = (request, response, method) => {
  const paramsPlace = method === 'get' ? 'query' : 'body';
  const apiName = request.params['apiName'];
  const apiCall = api[apiName];
  if (!apiCall || apiCall.method !== method) {
    return response.send(`no api with name ${ request.params.apiName } and method ${ method }`);
  }
  let exit = false;
  Object.keys(apiCall.parameters).forEach(param => {
    if (param[param.length - 1] === '?') {
      return;
    }
    if (!exit && request[paramsPlace][param]) {
      exit = response.send(`requires param ${ param }`) || true;
    }
  });
  if (exit) {
    return;
  }
  response.send(generate(omitFields(apiName, request[paramsPlace]['fields'])));
};

app.get('/:apiName', (request, response) => {
  return handler(request, response, 'get');
});

app.post('/:apiName', (request, response) => {
  return handler(request, response, 'post');
});

app.listen(PORT, err => {
  if (err) {
    console.log(err.message);
  } else {
    console.log(`server started on ${ PORT }`);
  }
});
