import { UserModel } from '../../db/scheme';
import { FullRequest, WSSubject } from './index';
import { SubjectSubscriptions, Subscription, UserSubscriptions } from './models';
import { Types } from 'mongoose';

let ObjectId = Types.ObjectId;

const DEFAULT_SUBSCRIPTION_NAME = 'defaultSubscription';

const userSubscriptions: UserSubscriptions = {};
const subjectSubscriptions: SubjectSubscriptions = {};

export const getUser = async userId => {
  const res = await UserModel.findById(ObjectId(userId)).populate('userDetails');
  if (res != null)
    res['getSubject'] = (subject, dynamicName = DEFAULT_SUBSCRIPTION_NAME) => userSubscriptions[userId + '']
      && userSubscriptions[userId + ''][subject] && userSubscriptions[userId + ''][subject][dynamicName]
      || new Subscription(subject, dynamicName);
  return res;
};

export const getSubject = (subject, dynamicName = DEFAULT_SUBSCRIPTION_NAME) => {
  return subjectSubscriptions[subject] && subjectSubscriptions[subject][dynamicName] || new Subscription(subject, dynamicName);
};

export const subscriptionCallBacks = (subject: WSSubject) => ({
  subscribe(fullRequest: FullRequest, dynamicName = DEFAULT_SUBSCRIPTION_NAME) {
    const user: any = fullRequest.connection.user;
    const userId = user._id + '';
    if (!user) return;
    if (!userSubscriptions[userId])
      userSubscriptions[userId] = {};
    if (!userSubscriptions[userId][subject.subject])
      userSubscriptions[userId][subject.subject] = {};
    if (!userSubscriptions[userId][subject.subject][dynamicName])
      userSubscriptions[userId][subject.subject][dynamicName] = new Subscription(subject.subject, dynamicName);
    userSubscriptions[userId][subject.subject][dynamicName].sockets.push(fullRequest.connection.socket);
    if (!subjectSubscriptions[subject.subject])
      subjectSubscriptions[subject.subject] = {};
    if (!subjectSubscriptions[subject.subject][dynamicName])
      subjectSubscriptions[subject.subject][dynamicName] = new Subscription(subject.subject, dynamicName);
    subjectSubscriptions[subject.subject][dynamicName].sockets.push(fullRequest.connection.socket);
  },
  unsubscribe(fullRequest: FullRequest, dynamicName = DEFAULT_SUBSCRIPTION_NAME) {
    const user: any = fullRequest.connection.user;
    const userId = user._id + '';
    if (!user) return;
    if (userSubscriptions[userId] &&
      userSubscriptions[userId][subject.subject] &&
      userSubscriptions[userId][subject.subject][dynamicName]) {
      userSubscriptions[userId][subject.subject][dynamicName].sockets =
        userSubscriptions[userId][subject.subject][dynamicName].sockets.filter(
          socket => socket.id != fullRequest.connection.socket.id
        );
      if (!userSubscriptions[userId][subject.subject][dynamicName].sockets.length)
        delete userSubscriptions[userId][subject.subject][dynamicName];
      if (!Object.keys(userSubscriptions[userId][subject.subject]).length)
        delete userSubscriptions[userId][subject.subject];
      if (!Object.keys(userSubscriptions[userId]).length)
        delete userSubscriptions[userId];
    }
    if (subjectSubscriptions[subject.subject] && subjectSubscriptions[subject.subject][dynamicName]) {
      subjectSubscriptions[subject.subject][dynamicName].sockets =
        subjectSubscriptions[subject.subject][dynamicName].sockets.filter(
          socket => socket.id != fullRequest.connection.socket.id
        );
      if (!subjectSubscriptions[subject.subject][dynamicName].sockets.length)
        delete subjectSubscriptions[subject.subject][dynamicName];
      if (!Object.keys(subjectSubscriptions[subject.subject]).length) {
        delete subjectSubscriptions[subject.subject];
      }
    }
    fullRequest.sendResponse(true);
  }
});
