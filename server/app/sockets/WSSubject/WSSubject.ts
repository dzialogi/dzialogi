import { subscriptionCallBacks } from './subscriptions';
import { Connection, FullRequest, Request } from './models';
import { checkToken } from '../../authMiddleware';

const subjects: WSSubject[] = [];

export class WSSubject {
  callBacks = subscriptionCallBacks(this);

  constructor(public subject, public requireAuthorization) {
    subjects.push(this);
  }

  subscribe(callBacks) {
    this.callBacks = {
      ...this.callBacks,
      ...callBacks
    };
    return this;
  }

  onRequest = async (request: Request, socket) => {
    const decoded: any = await checkToken(request.token);
    if (decoded.success || !this.requireAuthorization) {
      const connection = new Connection(decoded.user, socket);
      try {
        const callBackResult = this.callBacks[request.action] && this.callBacks[request.action](new FullRequest(
          (response, status = 200) => this.sendResponse(socket, request.id, response, status),
          connection
        ), request.data);
        if (callBackResult instanceof Promise) {
          await callBackResult;
        }
      } catch (e) {
        this.sendResponse(socket, request.id, e, 500);
      }
    } else {
      this.sendResponse(socket, request.id, decoded, 403);
    }
  };

  sendResponse = (socket, id, data, status) => {
    socket.emit(this.subject, {
      id,
      data,
      status,
      action: 'response'
    });
  };

  void() {

  }
}

export const initSubjectFor = socket => {
  subjects.forEach((subject: WSSubject) => socket.on(subject.subject, (request: Request) => subject.onRequest(request, socket)));
};
