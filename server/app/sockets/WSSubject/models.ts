import { Socket } from 'socket.io';
import { User } from '../../db/scheme';

export class Connection {
  constructor(
    public user: User,
    public socket: Socket
  ) {
  }
}

export class Request {
  constructor(
    public id: number,
    public action: string,
    public data: any,
    public token: string
  ) {
  }
}

export class FullRequest {
  constructor(
    public sendResponse: (response: any, status?: number) => void,
    public connection: Connection
  ) {
  }
}

export class Subscription {
  sockets: Socket[] = [];

  constructor(private subjectName: string, private dynamicName: string) {
  }

  emit(action, data) {
    this.sockets = this.sockets.filter(s => s.connected);
    this.sockets.forEach((socket: any) => {
      socket.emit(this.subjectName, {
        action,
        data,
        dynamicName: this.dynamicName
      });
    });
  }
}

export type UserSubscriptions = {
  [userId: string]: {
    [subject: string]: {
      [dynamicName: string]: Subscription
    }
  }
};

export type SubjectSubscriptions = {
  [subject: string]: {
    [dynamicName: string]: Subscription
  }
};
