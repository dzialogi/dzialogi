import socketio from 'socket.io';
import chatSocket from './chat';
import { initSubjectFor } from './WSSubject';

export const init = server => {
  const io = socketio(server);
  let sockets: any = {};

  io.on('connection', socket => {
    socket.on('userConnected', user => {
      sockets[user._id] = socket.id;
    });

    chatSocket(socket, io, sockets);
    initSubjectFor(socket);
  });
};