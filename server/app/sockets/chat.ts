import { MessageModel } from '../db/scheme';
import { removeUndefined } from '../utils';

export default function chatSocket(socket: any, io: any, sockets: any) {
  socket.on('getMessages', async (chatId: any, fn: any) => {
    const messages = await MessageModel.find({ chat: chatId }).populate({
      path: 'user',
      populate: { path: 'userDetails' }
    });
    fn(messages);
  });

  socket.on('newMessage', async (chatId: any, message: any) => {
    let params = removeUndefined({
      chat: chatId,
      text: message.text,
      user: message.user._id,
      status: 'S',
      date: message.createdAt,
      image: message.image
    });

    let newMessage = await MessageModel.create(params);

    newMessage = await newMessage.populate({ path: 'user', populate: { path: 'userDetails' } }).execPopulate();

    io.in('room ' + chatId).emit('messageReceived', newMessage);
  });
}
