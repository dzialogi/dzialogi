import jwt from 'jsonwebtoken';
import CONFIG from '../config';
import bcrypt from 'bcryptjs';
import { AnswerSegmentModel, UserModel, UserSegmentDataModel } from '../db/scheme';
import { FullRequest, WSSubject } from '../sockets/WSSubject';

const AuthSubject = new WSSubject('auth', false).subscribe({
  async register(req: FullRequest, { username, password }) {
    let passwordHash = bcrypt.hashSync(password, 10);

    let newUser: any = null;

    try {
      newUser = await UserModel.create({ username, password: passwordHash, userStatus: 'N' });
      newUser = await newUser.populate('userDetails').execPopulate();
      UserSegmentDataModel.insertMany((await AnswerSegmentModel.find({})).map(answerSegment => ({
        user: newUser,
        segment: answerSegment,
        value: 0
      })));
    } catch (e) {
      return req.sendResponse({
        success: false,
        message: e.message
      }, 403);
    }

    let token = jwt.sign({ id: newUser._id }, CONFIG.secret, { expiresIn: '30d' });

    return req.sendResponse({
      success: true,
      message: 'Authentication successful!',
      token: token,
      user: newUser,
    });
  },
  async login(req: FullRequest, { username, password }) {
    let user = await UserModel.findOne({ username }).populate('userDetails');

    if (!user || !bcrypt.compareSync(password || '', user.password)) {
      req.sendResponse({
        success: false,
        message: 'მომხმარებელი ან პაროლი არასწორია',
      }, 403);
      return;
    }

    let token = jwt.sign({ id: user._id }, CONFIG.secret, { expiresIn: '24h' });
    // return the JWT token for the future API calls
    req.sendResponse({
      success: true,
      message: 'Authentication successful!',
      token,
      user
    });
  },
  getLoggedUser(req: FullRequest) {
    req.sendResponse({
      success: !!req.connection.user,
      user: req.connection.user
    });
  }
});

export default AuthSubject;
