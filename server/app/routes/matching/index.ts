import { matchStatus, UserMatchDataModel, UserModel } from '../../db/scheme';
import { FunctionsPool, getUserName } from '../../utils';
import { matchesSortingFunction, updateMatches } from '../../workers/matchesWorkers';
import { FullRequest, getUser, WSSubject } from '../../sockets/WSSubject';
import { NotificationsService } from '../../utils/PushNotifications';

const updatePotentialMatches = async user => {
  const potentialMatches = await UserMatchDataModel.find({
    user1: user,
    status: matchStatus.POTENTIAL
  }).sort({ order: 1 });
  if (potentialMatches && potentialMatches.length) {
    potentialMatches[0].status = matchStatus.MATCHED;
    await potentialMatches[0].save();
    if (potentialMatches.length < 10) {
      updateMatches(user).then();
    }
  }
};

const matchesPool = new FunctionsPool();

const MatchesSubject = new WSSubject('matches', true).subscribe({
  async getMatchingList(req: FullRequest) {
    const matches: any = await UserMatchDataModel.find({
      user1: req.connection.user,
      status: matchStatus.MATCHED,
      rejected: false
    }).populate('user2');
    req.sendResponse(matches.map((match: any) => ({
      username: match.user2.username,
      personalMatch: match.personalMatch,
      politicalMatch: match.politicalMatch
    })).sort(matchesSortingFunction));
  },
  reject: matchesPool.push(async (req: FullRequest, username) => {
    const user: any = req.connection.user;
    try {
      const matchedUser = await UserModel.findOne({ username });
      const matchData: any = await UserMatchDataModel.find({
        $or: [{ user1: user, user2: matchedUser }, { user1: matchedUser, user2: user }],
        status: matchStatus.MATCHED,
      });
      if (!matchData || !matchData.length) {
        return req.sendResponse({ status: 'ok' });
      }
      const firstMatch = matchData.find(match => match.user1 + '' === user._id + '');
      firstMatch.rejected = true;
      await firstMatch.save();
      if (matchData.length === 2) {
        const secondMatch = matchData.find(match => match.user2 + '' === user._id + '');
        if (secondMatch) {
          await secondMatch.delete();
        }
      }
      if (matchedUser) {
        const target: any = await getUser(matchedUser._id);
        target.getSubject('matches').emit('delete', {
          username: user.username
        });
        target.getSubject('compass').emit('delete', {
          username: user.username
        });
      }
      await updatePotentialMatches(user);
      return req.sendResponse({ status: 'ok' });
    } catch (e) {
      return req.sendResponse({ status: 'FAILED' });
    }
  }),
  accept: matchesPool.push(async (req: FullRequest, username) => {
    const user: any = req.connection.user;
    try {
      const matchedUser: any = await UserModel.findOne({ username });
      const matchData: any = await UserMatchDataModel.find({
        $or: [{ user1: user, user2: matchedUser }, { user1: matchedUser, user2: user }],
        status: matchStatus.MATCHED,
      });
      if (!matchData || !matchData.length) {
        return req.sendResponse({ status: 'ok' });
      }
      const firstMatch = matchData.find(match => match.user1 + '' === user._id + '');
      firstMatch.status = matchStatus.OFFERED;
      await firstMatch.save();
      try {
        NotificationsService.sendTo(matchedUser._id + '', 'offer', getUserName(matchedUser), 'ახალი ძიალოგის შემოთავაზება', {}).then();
      } catch (e) {
      }
      if (matchData.length === 2) {
        const secondMatch = matchData.find(match => match.user2 + '' === user._id + '');
        if (secondMatch) {
          await secondMatch.delete();
        }
      }
      if (matchedUser) {
        const target: any = await getUser(matchedUser._id);
        target.getSubject('offers').emit('insert', {
          username: user.username,
          personalMatch: firstMatch.personalMatch,
          politicalMatch: firstMatch.politicalMatch
        });
        target.getSubject('matches').emit('delete', {
          username: user.username
        });
        target.getSubject('compass').emit('delete', {
          username: user.username
        });
      }
      await updatePotentialMatches(user);
      return req.sendResponse({ status: 'OFFERED' });
    } catch (e) {
      return req.sendResponse({ status: 'FAILED' });
    }
  })
});

export default MatchesSubject;
