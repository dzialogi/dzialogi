import { matchStatus, UserMatchDataModel, UserModel } from '../../db/scheme';
import { FunctionsPool } from '../../utils';
import { FullRequest, WSSubject } from '../../sockets/WSSubject';
import { updateChatsGlobal } from '../global';

const offersPool = new FunctionsPool();

const OffersSubject = new WSSubject('offers', true).subscribe({
  async getRequestsList(req: FullRequest) {
    const offers: any = await UserMatchDataModel.find({
      user2: req.connection.user,
      status: matchStatus.OFFERED,
      rejected: false
    }).populate('user1');
    req.sendResponse(offers.map((offer: any) => ({
      username: offer.user1.username,
      personalMatch: offer.personalMatch,
      politicalMatch: offer.politicalMatch
    })));
  },
  reject: offersPool.push(async (req: FullRequest, username) => {
    try {
      const offererUser = await UserModel.findOne({ username });
      const offer: any = await UserMatchDataModel.findOne({
        user1: offererUser,
        user2: req.connection.user,
        status: matchStatus.OFFERED,
      });
      offer.rejected = true;
      await offer.save();
      return req.sendResponse({ status: 'OK' });
    } catch (e) {
      return req.sendResponse({ status: 'FAILED' });
    }
  }),
  accept: offersPool.push(async (req: FullRequest, username) => {
    const user: any = req.connection.user;
    try {
      const offererUser: any = await UserModel.findOne({ username });
      const offer: any = await UserMatchDataModel.findOne({
        user1: offererUser,
        user2: user,
        status: matchStatus.OFFERED,
      });
      offer.status = matchStatus.CHATTING;
      await offer.save();
      updateChatsGlobal(user._id).then();
      updateChatsGlobal(offererUser._id).then();
      return req.sendResponse({ status: 'OK' });
    } catch (e) {
      return req.sendResponse({ status: 'FAILED' });
    }
  })
});

export default OffersSubject;
