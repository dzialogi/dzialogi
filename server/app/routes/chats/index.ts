import { matchStatus, MessageModel, UserMatchDataModel } from '../../db/scheme';
import { FullRequest, WSSubject } from '../../sockets/WSSubject';
import { Types } from 'mongoose';
import { updateChatsGlobal } from '../global';
import { getSubject } from '../../sockets/WSSubject/subscriptions';

export async function getChats(userId) {
  const chats = await UserMatchDataModel.find({
    $or: [
      { user1: userId },
      { user2: userId }
    ],
    status: matchStatus.CHATTING
  })
  .populate({ path: 'user1', populate: { path: 'userDetails' } })
  .populate({ path: 'user2', populate: { path: 'userDetails' } }).lean();

  for (let chat of chats) {
    let chatId = chat._id;

    let message = await MessageModel.findOne({ chat: chatId }).populate({
      path: 'user',
      populate: { path: 'userDetails' }
    }).sort({ date: -1 });

    message = message || null;

    chat.lastMessage = message;
  }
  return chats;
}

export const ChatSubject = new WSSubject('chat', true).subscribe({
  async getChats(request: FullRequest) {
    const user: any = request.connection.user;
    const chats = await getChats(user._id);
    return request.sendResponse(chats);
  },
  async setAgreement(request: FullRequest, { chatId, agreement = true }) {
    const user: any = request.connection.user;
    let chat: any = await UserMatchDataModel.findById(Types.ObjectId(chatId));

    let isUser1 = chat.user1 + '' === user._id + '';
    let updateProps = isUser1 ? { agreement1: agreement } : { agreement2: agreement };
    chat = await UserMatchDataModel.findOneAndUpdate({ _id: chatId }, updateProps, { new: true });

    updateChatsGlobal(chat.user1 + '').then();
    updateChatsGlobal(chat.user2 + '').then();

    getSubject('messenger', chatId + '_' + user._id).emit('chatAgreeUpdated', {
      agreement1: chat.agreement1,
      agreement2: chat.agreement2
    });

    return request.sendResponse(true);
  }
});

export default ChatSubject;
