import {
  AnswerEffectModel,
  PostModel,
  PostReactsModel,
  PostTypeModel,
  QuizAnswerModel,
  QuizLogModel,
  QuizQuestionModel,
  QuizTypeModel,
  UserSegmentDataModel,
  UserStatus
} from '../../db/scheme';
import { updateMatches } from '../../workers/matchesWorkers';
import { FullRequest, WSSubject } from '../../sockets/WSSubject';
import { getSubject } from '../../sockets/WSSubject/subscriptions';

const QuizSubject = new WSSubject('quiz', true).subscribe({
  async getFirstQuestion(req: FullRequest, quizType) {
    quizType = await QuizTypeModel.findOne({ type: quizType });
    // @ts-ignore
    const question = await QuizQuestionModel.findOne({ isStarter: true, quizType });

    if (!question) {
      return req.sendResponse({ question: null });
    }
    // @ts-ignore
    const answers = await QuizAnswerModel.find({ question: question._id });
    // @ts-ignore
    return req.sendResponse({ _id: question._id, question: question.question, answers: answers });
  },
  async sendAnswer(req: FullRequest, answerIds) {
    const user = req.connection.user;
    const answers = await QuizAnswerModel.find({ _id: { $in: answerIds } });
    if (!answers.length) return req.sendResponse({ message: 'Unknown error' }, 500);
    await removeOldLog(answers[0].question, req.connection.user);
    const questions: any[] = [];
    for (let answer of answers) {
      // @ts-ignore
      const question = await QuizQuestionModel.findOne({ _id: answer.question });
      logAnswer(answer, question, req.connection.user).then();
      await answerEffect(answer, req.connection.user);
      // @ts-ignore
      const nextQuestion = await QuizQuestionModel.findOne({ _id: answer.nextQuestion });
      // @ts-ignore
      if (nextQuestion) {
        const answers = await QuizAnswerModel.find({ question: nextQuestion._id });
        questions.push({ _id: nextQuestion._id, question: nextQuestion.question, answers: answers });
      }
    }
    postAnswersOnProfile(answers, answers[0].question, user).then();
    if (!questions.length) {
      const user: any = req.connection.user;
      user.userStatus = UserStatus.ACTIVE;
      user.save();
      updateMatches().then();
    }
    return req.sendResponse({ questions });
  }
});

async function removeOldLog(question: any, user: any) {
  const quizLog = await QuizLogModel.find({ question, user });
  for (let log of quizLog) {
    (await AnswerEffectModel.find({ answer: log.answer })).forEach(async answerEffect => {
      const userSegmentData = await UserSegmentDataModel.findOne({ user: user, segment: answerEffect.segment });
      if (userSegmentData) {
        userSegmentData.value = userSegmentData.value - answerEffect.effect;
        userSegmentData.save();
      }
    });
    log.remove();
  }
}

async function logAnswer(answer: any, question: any, user: any) {
  QuizLogModel.create({ user, question, answer }).then();
}

async function postAnswersOnProfile(answers: any[], question: any, user: any) {
  PostModel.create({
    user,
    date: new Date(),
    text: 'quiz',
    postType: await PostTypeModel.findOne({ type: 'quiz' }),
    quizQuestion: question.question,
    quizAnswer: answers.map(answer => answer.answer.trim()).join(', ')
  }).then(async (post: any) => {
    post = await PostModel.findById(post._id)
    .populate({ path: 'user', populate: { path: 'userDetails' } })
    .populate('postType').lean();
    post.reacts = await PostReactsModel.find({ post: post._id });
    getSubject('posts', user._id + '').emit('postAdded', post);
  });
}

async function answerEffect(answer: any, user: any) {
  (await AnswerEffectModel.find({ answer })).forEach(async answerEffect => {
    const userSegmentData = await UserSegmentDataModel.findOne({ user: user, segment: answerEffect.segment });
    if (!userSegmentData) {
      UserSegmentDataModel.create({
        user,
        segment: answerEffect.segment,
        value: answerEffect.effect
      });
    } else {
      userSegmentData.value = answerEffect.effect + userSegmentData.value;
      await userSegmentData.save();
    }
  });
}

export default QuizSubject;
