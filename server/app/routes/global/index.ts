import { WSSubject } from '../../sockets/WSSubject';
import { getSubject } from '../../sockets/WSSubject/subscriptions';
import { getChats } from '../chats';

const GlobalSubject = new WSSubject('global', true).subscribe({});

export default GlobalSubject;

export async function updateChatsGlobal(userId) {
  let chats = await getChats(userId);
  getSubject('global', userId).emit('chatsUpdated', chats);
}
