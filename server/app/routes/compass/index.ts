import { matchStatus, UserMatchDataModel, UserSegmentDataModel } from '../../db/scheme';
import { FullRequest, WSSubject } from '../../sockets/WSSubject';

const N_COMPASS = 15;
export const getCompassListFor = async user => {
  const mapFnc = (match: any) => ({
    user: (match.user1._id + '' === user._id + '' ? match.user2 : match.user1),
    personalMatch: match.personalMatch,
    politicalMatch: match.politicalMatch
  });
  const connectedUsers: any = (await UserMatchDataModel.find({
    $or: [{ user1: user }, { user2: user }],
    status: matchStatus.CHATTING, agreement1: true, agreement2: true
  }).populate('user2').populate('user1')).map(mapFnc);

  const chatUsers: any = (await UserMatchDataModel.find({
    $and: [
      { $or: [{ user1: user }, { user2: user }] },
      { $or: [{ agreement1: false }, { agreement2: false }] }
    ],
    status: matchStatus.CHATTING,
  }).populate('user2').populate('user1')).map(mapFnc);

  const matcherDataLimit = connectedUsers.length + chatUsers.length > N_COMPASS ? 2 : N_COMPASS - (connectedUsers.length + chatUsers.length) + 2;
  const matchedUsers: any = (await UserMatchDataModel.find({
    user1: user, status: matchStatus.MATCHED
  }).limit(matcherDataLimit).populate('user2').populate('user1')).map(mapFnc);

  const data = await UserSegmentDataModel.find({ user: { $in: [user, ...matchedUsers.map(m => m.user), ...chatUsers.map(m => m.user), ...connectedUsers.map(m => m.user)] } }).populate({ path: 'segment' });

  const transform = (users, isConnected, isChat) => users.map(m => ({
    username: m.user.username,
    ...getCoordinates(m.user, data),
    personalMatch: m.personalMatch,
    politicalMatch: m.politicalMatch,
    isConnected,
    isChat
  }));

  const matches = [
    ...transform(connectedUsers, true, true),
    ...transform(chatUsers, false, true),
    ...transform(matchedUsers, false, false)
  ];

  const mainPoint = {
    matches,
    ...getCoordinates(user, data)
  };
  return mainPoint;
};

const CompassSubject = new WSSubject('compass', true).subscribe({
  async getCompassList(req: FullRequest) {
    req.sendResponse(await getCompassListFor(req.connection.user));
  }
});

function getCoordinates(user, data: any[]) {
  const res = {};
  data = data.filter(item => item.user + '' === user._id + '');
  data.forEach((segment: any) => {
    if (!segment.segment.isPersonal) {
      res[segment.segment.segment] = segment.value;
    }
  });
  return res;
}

export default CompassSubject;