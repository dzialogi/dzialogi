import { UserDetailsModel, UserModel } from '../../db/scheme';
import { FullRequest, WSSubject } from '../../sockets/WSSubject';
import { Types } from 'mongoose';
import bcrypt from 'bcryptjs';

const UsersSubject = new WSSubject('users', false).subscribe({
  async getUserByUsername(request: FullRequest, username) {
    const user = await UserModel.findOne({
      username
    })
    .populate('userDetails');
    return request.sendResponse(user);
  },
  async getUserById(request: FullRequest, userId) {
    const user = await UserModel.findById(Types.ObjectId(userId)).populate('userDetails');
    return request.sendResponse(user);
  },
  async updateUser(request: FullRequest, { userId, firstName, lastName, password, confirmPassword, avatar }) {
    let user: any = await UserModel.findById(Types.ObjectId(userId)).lean();
    let userDetails: any = user.userDetails;
    if (!userDetails) {
      userDetails = await UserDetailsModel.create({ firstName, lastName, avatar });
      user.userDetails = userDetails._id;
      user = await UserModel.findOneAndUpdate({ _id: userId }, user).lean();
    } else {
      await UserDetailsModel.findOneAndUpdate({ _id: userDetails }, {
        firstName,
        lastName,
        avatar
      }).lean();
      userDetails = await UserDetailsModel.findById(Types.ObjectId(userDetails));
    }
    if (password)
      await UserModel.findOneAndUpdate({ _id: userId }, { password: bcrypt.hashSync(password, 10) });
    user.userDetails = userDetails;
    return request.sendResponse(user);
  }
});

export default UsersSubject;
