import { MessageModel, UserMatchDataModel, UserModel } from '../../db/scheme';
import { FullRequest, WSSubject } from '../../sockets/WSSubject';
import { getUserName, removeUndefined } from '../../utils';
import { getSubject } from '../../sockets/WSSubject/subscriptions';
import { updateChatsGlobal } from '../global';
import { NotificationsService } from '../../utils/PushNotifications';
import { Types } from 'mongoose';

const MessengerSubject = new WSSubject('messenger', true).subscribe({
  async getMessages(request: FullRequest, { chatId, from = 0, count = 20 }) {
    const user: any = request.connection.user;

    const messages = await MessageModel.find({ chat: chatId }).populate({
      path: 'user',
      populate: { path: 'userDetails' }
    }).sort({ date: -1 }).skip(from).limit(count);

    return request.sendResponse(messages.reverse());
  },
  async getChat(request: FullRequest, chatId) {
    request.sendResponse(await UserMatchDataModel.findById(Types.ObjectId(chatId))
    .populate({ path: 'user1', populate: { path: 'userDetails' } })
    .populate({ path: 'user2', populate: { path: 'userDetails' } }));
  },
  async getChatByUsername(request: FullRequest, username) {
    const targetUser: any = await UserModel.findOne({ username });
    const user: any = request.connection.user;
    request.sendResponse(await UserMatchDataModel.findOne({
      $or: [
        { user1: Types.ObjectId(targetUser._id), user2: Types.ObjectId(user._id) },
        { user2: Types.ObjectId(targetUser._id), user1: Types.ObjectId(user._id) }
      ]
    })
    .populate({ path: 'user1', populate: { path: 'userDetails' } })
    .populate({ path: 'user2', populate: { path: 'userDetails' } }));
  },
  async addMessage(request: FullRequest, { chatId, message, partnerId }) {
    const user: any = request.connection.user;

    let params = removeUndefined({
      chat: chatId,
      text: message.text,
      user: message.user._id,
      status: 'S',
      date: message.createdAt,
      image: message.image
    });

    let newMessage = await MessageModel.create(params);

    newMessage = await newMessage.populate({ path: 'user', populate: { path: 'userDetails' } }).execPopulate();

    getSubject('messenger', chatId + '_' + user._id).emit('messageAdded', newMessage);
    NotificationsService.sendTo(
      partnerId,
      'chat',
      getUserName(user),
      message.text,
      { chatId }
    ).then();

    updateChatsGlobal(user._id).then();
    updateChatsGlobal(partnerId).then();

    return request.sendResponse(newMessage);
  },
});

export default MessengerSubject;
