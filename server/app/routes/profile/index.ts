import { FullRequest, WSSubject } from '../../sockets/WSSubject';
import { PostModel, PostReactsModel, PostTypeModel, ReactModel, UserModel } from '../../db/scheme';
import { getSubject } from '../../sockets/WSSubject/subscriptions';
import { Types } from 'mongoose';

const PostsSubject = new WSSubject('posts', true).subscribe({
  async getPosts(request: FullRequest, { username = '', from = 0, count = 10 }) {
    let user: any = request.connection.user;
    if (username)
      user = await UserModel.findOne({ username }).populate('userDetails');

    let posts: any = await PostModel.find({
      user: user._id
    }).sort({ date: -1 }).skip(from).limit(count)
    .populate({ path: 'user', populate: { path: 'userDetails' } })
    .populate('postType').lean();

    for (let post of posts) {
      const reacts = await PostReactsModel.find({ post: post._id });
      post.reacts = reacts;
    }

    return request.sendResponse(posts);
  },
  post: async (request: FullRequest, text: string) => {
    const user: any = request.connection.user;

    let textPostType = await PostTypeModel.findOne({ type: 'text' }) || { _id: '' };

    let newPost: any = await PostModel.create({
      user: user._id,
      date: new Date(),
      text,
      postType: textPostType._id
    });

    newPost = await PostModel.findById(Types.ObjectId(newPost._id)).populate({
      path: 'user',
      populate: { path: 'userDetails' }
    }).populate('postType').lean();

    newPost.reacts = [];

    getSubject('posts', user._id).emit('postAdded', newPost);

    return request.sendResponse(newPost);

  },
  addLike: async (request: FullRequest, postId) => {
    const user: any = request.connection.user;
    let likeReact: any = await ReactModel.findOne({ type: 'like' });
    let newReact: any = await PostReactsModel.create({ user: user._id, react: likeReact._id, post: postId });
    let post: any = await PostModel.findById(Types.ObjectId(postId));

    getSubject('posts', post.user).emit('likeAdded', { postId: postId, like: newReact });

    return request.sendResponse(newReact);
  },

  removeLike: async (request: FullRequest, postId) => {
    const user: any = request.connection.user;
    await PostReactsModel.deleteOne({ user: user._id, post: postId });
    let post: any = await PostModel.findById(Types.ObjectId(postId));

    getSubject('posts', post.user).emit('likeRemoved', { postId: post._id, userId: user._id });

    return request.sendResponse(true);
  },

  delete: async (request: FullRequest, id) => {
    const user: any = request.connection.user;

    await PostModel.findByIdAndDelete(Types.ObjectId(id));

    getSubject('posts', user._id).emit('postDeleted', id);

    return request.sendResponse(true);
  },
});

export default PostsSubject;
