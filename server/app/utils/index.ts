export function removeUndefined(srcObj: any) {
  let obj = { ...srcObj };
  for (let key in obj) {
    if (!obj.hasOwnProperty(key)) continue;

    if (obj[key] === undefined)
      delete obj[key];
  }
  return obj;
}

export class FunctionsPool {
  functions: [Function, { resolve: any }, ...any[]][] = [];
  working: boolean = false;

  push = callBack => {
    return async (...params) => {
      const resolver: { resolve: any } = { resolve: null };
      const requestPromise = new Promise((resolve) => resolver.resolve = resolve);
      this.functions.push([callBack, resolver, ...params]);
      this.callNext().then();
      return await requestPromise;
    };
  };

  callNext = async () => {
    if (this.working || !this.functions.length) return;
    this.working = true;
    // @ts-ignore
    const [callBack, resolver, ...params] = this.functions.splice(0, 1)[0];
    try {
      if (callBack) resolver.resolve(await callBack(...params));
    } catch (e) {
      throw e;
    } finally {
      this.working = false;
      this.callNext().then();
    }
  };
}

export function getUserName(user) {
  let name = user.username;
  try {
    if (user.userDetails && user.userDetails.firstName)
      name = user.userDetails.firstName;

    if (user.userDetails && user.userDetails.lastName)
      name += ' ' + user.userDetails.lastName;
  } catch (e) {
  }
  return name;
}