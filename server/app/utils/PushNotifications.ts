import Expo from 'expo-server-sdk';
import { FullRequest, WSSubject } from '../sockets/WSSubject';
import { checkToken } from '../authMiddleware';
import { NotificationDevice, NotificationDeviceModel } from '../db/scheme';
import { Types } from 'mongoose';

let expo = new Expo();

async function unregisterUser(notificationToken) {
  const doc = { notificationToken };
  await NotificationDeviceModel.updateOne(doc, doc);
}

export const NotificationsService = (new class NotificationsService {
  private static async send(to, channel, data, title, body) {
    return await expo.sendPushNotificationsAsync([{
      priority: 'high',
      to,
      data: {
        channel,
        ...data
      },
      title,
      body,
      sound: 'default',
      channelId: 'pushChannel'
    }]);
  }

  async sendTo(userId, channel, title, body, data) {
    const user = Types.ObjectId(userId);
    const notificationDevices: NotificationDevice[] = await NotificationDeviceModel.find({ user });
    if (!notificationDevices) return;
    for (let device of notificationDevices) {
      const { notificationToken: to, authToken } = device;
      if (!(await checkToken(authToken)).success) {
        unregisterUser(to).then();
        continue;
      }
      NotificationsService.send(to, channel, data, title, body).then();
    }
  }

  async publish(channel, title, body, data) {
    const notificationDevices: NotificationDevice[] = await NotificationDeviceModel.find({});
    if (!notificationDevices) return;
    for (let device of notificationDevices) {
      NotificationsService.send(device.notificationToken, channel, data, title, body).then();
    }
  }
});

const PushNotificationsSubject = new WSSubject('pushNotification', false).subscribe({
  async registerToken(req: FullRequest, notificationToken) {
    try {
      await NotificationDeviceModel.create({
        notificationToken
      });
    } catch (e) {
    } finally {
      req.sendResponse({
        status: 'OK'
      });
    }
  },
  async registerUser(req: FullRequest, { notificationToken, authToken }) {
    const user: any = req.connection.user;
    if (!user) req.sendResponse({
      status: 'UNAUTHORIZED'
    }, 403);
    try {
      let notification = await NotificationDeviceModel.findOne({
        notificationToken
      });
      if (!notification) {
        notification = await NotificationDeviceModel.create({
          notificationToken,
          authToken,
          user: user._id
        });
      } else {
        notification.user = user._id;
        notification.authToken = authToken;
        notification.save();
      }
    } catch (e) {
    } finally {
      req.sendResponse({
        status: 'OK'
      });
    }
  },
  async unregisterUser(req: FullRequest, notificationToken) {
    if (!req.connection.user) req.sendResponse({
      status: 'UNAUTHORIZED'
    }, 403);
    try {
      await unregisterUser(notificationToken);
    } catch (e) {
    } finally {
      req.sendResponse({
        status: 'OK'
      });
    }
  }
});

export default PushNotificationsSubject;