import { prop, Ref, Typegoose } from 'typegoose';
import connect from './connect';
import { Subscription } from '../sockets/WSSubject/models';

connect();

export enum UserStatus {
  NEW = 'N',
  PENDING = 'P',
  ACTIVE = 'A',
  CLOSED = 'C'
}

class UserDetails extends Typegoose {
  @prop()
  firstName?: string;

  @prop()
  lastName?: string;

  @prop()
  birthDate?: Date;

  @prop()
  country?: string;

  @prop()
  city?: string;

  @prop()
  avatar?: string;

  @prop()
  coordinateX?: number;

  @prop()
  coordinateY?: number;

  @prop()
  rating?: number;
};

export class User extends Typegoose {
  @prop({ ref: UserDetails })
  userDetails: Ref<UserDetails>;

  @prop({ required: true, unique: true })
  username: string;

  @prop({ required: true })
  password: string;

  @prop({ required: true, enum: UserStatus })
  userStatus: string;

  getSubject: (subject, params?) => Subscription;
}

class SecurityQuestion extends Typegoose {
  @prop({ required: true })
  question: string;
}

class SecurityDetails extends Typegoose {
  @prop({ ref: User, required: true })
  user: Ref<User>;

  @prop({ required: true, ref: SecurityQuestion })
  question: Ref<SecurityQuestion>;

  @prop({ required: true })
  answer: string;
}

class QuizType extends Typegoose {
  @prop({ required: true, unique: true })
  type: string;

  @prop({ required: true })
  description: string;
}

enum QuestionStatus {
  ACTIVE = 'A',
  CLOSED = 'D'
}

class QuizQuestion extends Typegoose {
  @prop({ required: true, ref: QuizType })
  quizType: Ref<QuizType>;

  @prop({ required: true, unique: true })
  question: string;

  @prop({ required: true, enum: QuestionStatus })
  status: string;

  @prop({ required: false })
  isStarter: boolean;
}

class QuizAnswer extends Typegoose {
  @prop({ required: true, ref: QuizQuestion })
  question: Ref<QuizQuestion>;

  @prop({ required: true })
  answer: string;

  @prop({ ref: QuizQuestion })
  nextQuestion?: Ref<QuizQuestion>;
}

class QuizLog extends Typegoose {
  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true, ref: QuizQuestion })
  question: Ref<QuizQuestion>;

  @prop({ required: true, ref: QuizAnswer })
  answer: Ref<QuizAnswer>;
}

class AnswerSegment extends Typegoose {
  @prop({ required: true, unique: true })
  segment: string;

  @prop({ required: true })
  description: string;

  @prop({ required: true })
  isPersonal: boolean;
}

class AnswerEffect extends Typegoose {
  @prop({ required: true, ref: QuizAnswer })
  answer: Ref<QuizAnswer>;

  @prop({ required: true, ref: AnswerSegment })
  segment: Ref<AnswerSegment>;

  @prop({ required: true })
  effect: number;
}

export class UserSegmentData extends Typegoose {
  @prop({ ref: User, required: true })
  user: Ref<User>;

  @prop({ required: true, ref: AnswerSegment })
  segment: Ref<AnswerSegment>;

  @prop({ required: true })
  value: number;
}

export enum matchStatus {
  POTENTIAL = 'P',
  MATCHED = 'M',
  OFFERED = 'O',
  CHATTING = 'C'
}


export class UserMatchData extends Typegoose {
  @prop({ ref: User, required: true })
  user1: Ref<User>;

  @prop({ ref: User, required: true })
  user2: Ref<User>;

  @prop({ required: true })
  personalMatch: number;

  @prop({ required: true })
  politicalMatch: number;

  @prop({ required: true, default: false })
  rejected: boolean;

  @prop({ required: true, enum: matchStatus })
  status: string;

  @prop({ required: true, default: 0 })
  order: number;

  @prop({ required: true, default: false })
  agreement1: boolean;

  @prop({ required: true, default: false })
  agreement2: boolean;
}

class PostType extends Typegoose {
  @prop({ required: true, unique: true })
  type: string;

  @prop({ required: true })
  description: string;
}

class Post extends Typegoose {
  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true })
  text: string;

  @prop({ required: true })
  date: Date;

  @prop({ required: true, ref: PostType })
  postType: Ref<PostType>;

  @prop({ required: false })
  quizQuestion: string;

  @prop({ required: false })
  quizAnswer: string;
}

class React extends Typegoose {
  @prop({ required: true, unique: true })
  type: string;

  @prop({ required: true })
  description: string;
}

class PostReacts extends Typegoose {
  @prop({ required: true, ref: Post })
  post: Ref<Post>;

  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true, ref: React })
  react: Ref<React>;
}

class Comment extends Typegoose {
  @prop({ required: true, ref: Post })
  post: Ref<Post>;

  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true })
  text: string;

  @prop({ required: true })
  date: Date;
}

class CommentReact extends Typegoose {
  @prop({ required: true, ref: Comment })
  comment: Ref<Comment>;

  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true, ref: React })
  react: Ref<React>;
}

class Chat extends Typegoose {
  @prop({ required: true, ref: User })
  firstUser: Ref<User>;

  @prop({ required: true, ref: User })
  secondUser: Ref<User>;

  @prop({ required: true, default: false })
  firstUserAgree: boolean;

  @prop({ required: true, default: false })
  secondUserAgree: boolean;
}

export enum MessageStatus {
  'SENT' = 'S',
  'DELIVERED' = 'D',
  'READ' = 'R'
}

class Message extends Typegoose {
  @prop({ required: true, ref: Chat })
  chat: Ref<Chat>;

  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop()
  text?: string;

  @prop({ required: true, enum: MessageStatus })
  status: string;

  @prop()
  file?: string;

  @prop()
  image?: string;

  @prop({ required: true })
  date: Date;

  @prop({ required: true, default: false })
  deleted: boolean;

  @prop({ required: true, default: false })
  isHandshake: boolean;
}

class Review extends Typegoose {
  @prop({ required: true, ref: Chat })
  chat: Ref<Chat>;

  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true })
  review: number;
}

class Match extends Typegoose {
  @prop({ required: true, ref: User })
  user: Ref<User>;

  @prop({ required: true, ref: User })
  matchedUser: Ref<User>;

  @prop({ required: true })
  politicalMatch: number;

  @prop({ required: true })
  personalMatch: number;

  @prop({ required: true, default: false })
  rejected: boolean;
}

class Offer extends Typegoose {
  @prop({ required: true, ref: User })
  offererUser: Ref<User>;

  @prop({ required: true, ref: User })
  targetUser: Ref<User>;

  @prop({ required: true })
  politicalMatch: number;

  @prop({ required: true })
  personalMatch: number;

  @prop({ required: true, default: false })
  rejected: boolean;
}

export class NotificationDevice extends Typegoose {
  @prop({ required: true, unique: true })
  notificationToken: string;

  @prop({ required: false, ref: User })
  user: Ref<User>;

  @prop({ required: false })
  authToken: string;
}

export const UserModel = new User().getModelForClass(User);
export const UserDetailsModel = new UserDetails().getModelForClass(UserDetails);
export const SecurityQuestionModel = new SecurityQuestion().getModelForClass(SecurityQuestion);
export const SecurityDetailsModel = new SecurityDetails().getModelForClass(SecurityDetails);
export const QuizTypeModel = new QuizType().getModelForClass(QuizType);
export const QuizQuestionModel = new QuizQuestion().getModelForClass(QuizQuestion);
export const QuizAnswerModel = new QuizAnswer().getModelForClass(QuizAnswer);
export const QuizLogModel = new QuizLog().getModelForClass(QuizLog);
export const AnswerSegmentModel = new AnswerSegment().getModelForClass(AnswerSegment);
export const AnswerEffectModel = new AnswerEffect().getModelForClass(AnswerEffect);
export const UserSegmentDataModel = new UserSegmentData().getModelForClass(UserSegmentData);
export const PostTypeModel = new PostType().getModelForClass(PostType);
export const PostModel = new Post().getModelForClass(Post);
export const ReactModel = new React().getModelForClass(React);
export const PostReactsModel = new PostReacts().getModelForClass(PostReacts);
export const CommentModel = new Comment().getModelForClass(Comment);
export const CommentReactModel = new CommentReact().getModelForClass(CommentReact);
export const ChatModel = new Chat().getModelForClass(Chat);
export const MessageModel = new Message().getModelForClass(Message);
export const ReviewModel = new Review().getModelForClass(Review);
export const MatchModel = new Match().getModelForClass(Match);
export const OfferModel = new Offer().getModelForClass(Offer);
export const UserMatchDataModel = new UserMatchData().getModelForClass(UserMatchData);
export const NotificationDeviceModel = new NotificationDevice().getModelForClass(NotificationDevice);
