import jwt from 'jsonwebtoken';
import CONFIG from './config';
import { getUser } from './sockets/WSSubject';

export let checkLogged = (req: any, res: any, next: any) => {
  let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

  checkToken(token).then((result: any) => {
    if (result.success) {
      req.user = result.user;
      next();
    } else {
      return res.status(403).send(result);
    }
  });
};

export const checkToken = async token => {
  const error = message => ({ success: false, message });
  if (token) {
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }
    try {
      const decoded: any = jwt.verify(token, CONFIG.secret);
      return {
        success: true,
        user: await getUser(decoded.id)
      };
    } catch (err) {
      return error('Token is not valid');
    }
  } else {
    return error('Auth token is not supplied');
  }
};