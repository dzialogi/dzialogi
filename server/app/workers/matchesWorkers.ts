import { matchStatus, UserMatchDataModel, UserModel, UserSegmentDataModel } from '../db/scheme';
import { FunctionsPool } from '../utils';

const N_MATCHES = 10;

function normalizedValue(value: number, maxValue: number, minValue: number) {
  return (value - minValue) / (maxValue - minValue);
}

export const matchesSortingFunction = (a: any, b: any) =>
  a !== undefined && b !== undefined ?
    a.personalDiff !== undefined ?
      ((a.personalDiff * 0.7) - (a.politicalDiff * 0.3)) - ((b.personalDiff * 0.7) - (b.politicalDiff * 0.3)) :
      ((b.personalMatch * 0.7) - (b.politicalMatch * 0.3)) - ((a.personalMatch * 0.7) - (a.politicalMatch * 0.3)) :
    0;

async function fetchDatabase(loggedUser) {
  const users = (await UserModel.find({ _id: { $nin: [loggedUser] } })),
    userSegmentData = await UserSegmentDataModel.find({}).populate({ path: 'segment' }),
    otherUsersData = {}, sort = (a, b) => a.segment.segment < b.segment.segment ? -1 : 1;
  let loggedUserData = userSegmentData.filter(userData => userData.user + '' === loggedUser._id + '');
  userSegmentData.forEach(userData => {
    if (userData.user + '' !== loggedUser._id + '') {
      if (!otherUsersData[userData.user + '']) otherUsersData[userData.user + ''] = {};
      // @ts-ignore
      otherUsersData[userData.user + ''][userData.segment.segment] = (userData);
    }
  });
  loggedUserData.sort(sort);
  return [users, loggedUserData, otherUsersData];
}

async function addUserSegment(user, segment, value = 0) {
  return (await (await UserSegmentDataModel.create({
    user, segment, value
  })).populate('segment'));
}

async function getMatches(loggedUser) {
  let matchedList = [], minValue = Infinity, maxValue = -Infinity;
  const [users = [], loggedUserData = [], otherUsersData = {}] = await fetchDatabase(loggedUser);
  // @ts-ignore
  for (let user of users) {
    let [personalDiff, personalCount, politicalDiff, politicalCount] = [0, 0, 0, 0];
    let i = 0;
    if (!otherUsersData[user._id + '']) continue;
    // @ts-ignore
    for (let currSegmentData of loggedUserData) {
      let targetSegmentData: any = otherUsersData[user._id + ''][currSegmentData.segment.segment];
      i++;
      if (!targetSegmentData) targetSegmentData = await addUserSegment(user, currSegmentData.segment);
      if (targetSegmentData.segment.isPersonal) {
        personalCount++;
        personalDiff = personalDiff + Math.pow(targetSegmentData.value - currSegmentData.value, 2);
      } else {
        politicalCount++;
        politicalDiff = politicalDiff + Math.pow(targetSegmentData.value - currSegmentData.value, 2);
      }
    }
    [personalDiff, politicalDiff] = [personalCount ? Math.sqrt(personalDiff / personalCount) : 0, politicalCount ? Math.sqrt(politicalDiff / politicalCount) : 0];
    [maxValue, minValue] = [Math.max(maxValue, personalDiff, politicalDiff), Math.min(minValue, personalDiff, politicalDiff)];
    matchedList.push({
      // @ts-ignore
      username: user.username, user: user._id, personalDiff, politicalDiff
    });
  }
  maxValue *= 1.2;
  minValue *= 0.8;
  matchedList.forEach((match: any) => {
    match.personalDiff = normalizedValue(match.personalDiff, maxValue, minValue) || 0;
    match.politicalDiff = normalizedValue(match.politicalDiff, maxValue, minValue) || 0;
  });
  let matches: any[] = [];
  for (let { user, personalDiff, politicalDiff } of matchedList) {
    const entries = await UserMatchDataModel.find({
      $or: [{ user1: loggedUser, user2: user }, { user1: user, user2: loggedUser }]
    });
    if (!entries || !entries.length || entries.some(entry => !entry.rejected && [matchStatus.MATCHED + '', matchStatus.POTENTIAL + ''].includes(entry.status))) {
      matches.push({
        user1: loggedUser,
        user2: user,
        personalMatch: 1 - personalDiff,
        politicalMatch: 1 - politicalDiff
      });
    } else {
      entries[0].personalMatch = 1 - personalDiff;
      entries[0].politicalMatch = 1 - politicalDiff;
      entries[0].save();
    }
  }
  matches.sort(matchesSortingFunction);
  matches.forEach((match, index) => {
    index > 30 ? (match.status = matchStatus.POTENTIAL) : (match.status = matchStatus.MATCHED);
    match.order = index;
  });
  matches = matches.slice(0, 50);
  await UserMatchDataModel.insertMany(matches).then();
  return matches.length;
}

async function updateMatchesFor(user) {
  // STEP ONE: Delete all entries for this user which are not rejected and have status M or P
  const { n: deleteCount } = await UserMatchDataModel.deleteMany({
    user1: user,
    rejected: false,
    status: {
      $in: [matchStatus.MATCHED, matchStatus.POTENTIAL]
    }
  });
  const matchesCount = await getMatches(user);
  return { deleteCount, matchesCount };
}

const updateMatchesPool = new FunctionsPool();
export const updateMatches = updateMatchesPool.push(function (user?) {
  return new Promise(async resolve => {
    let done = 0;
    const users = (user ? [user] : await UserModel.find({}));
    users.forEach(async user => {
      try {
        await updateMatchesFor(user);
      } catch (e) {
        throw e;
      } finally {
        done++;
        if (done === users.length) {
          resolve();
        }
      }
    });
  });
});

const intervalHandle = async () => {
  const startTime = new Date();
  await updateMatches();
  // @ts-ignore
  console.log('updated users data in', ((new Date() - startTime) / 1000).toFixed(2), 'seconds');
};

intervalHandle().then();
// Start updating matches for all users every 10 minutes
setInterval(intervalHandle, 600000);