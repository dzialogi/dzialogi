import { matchStatus, MessageModel, MessageStatus, UserMatchDataModel } from '../db/scheme';

export async function sendHandshakeMessage(user1, user2) {
  const chat: any = (await UserMatchDataModel.find({
    user1: { $in: [user1, user2] },
    user2: { $in: [user1, user2] },
    status: matchStatus.CHATTING
  })).filter(a => a.user1 + '' !== a.user2 + '').reduce((_, a: any) => a, null);
  MessageModel.create({
    chat,
    user: null,
    status: MessageStatus.SENT,
    date: new Date(),
    isHandshake: true
  });
}

