import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import './workers/matchesWorkers';
import { init as socketInit } from './sockets';
import QuizSubject from './routes/quiz';
import UsersSubject from './routes/users';
import ChatSubject from './routes/chats';
import CompassSubject from './routes/compass';
import MatchesSubject from './routes/matching';
import AuthSubject from './routes/auth';
import OffersSubject from './routes/offers';
import PostsSubject from './routes/profile';
import MessengerSubject from './routes/messenger';
import GlobalSubject from './routes/global';
import PushNotificationsSubject from './utils/PushNotifications';

const app: express.Application = express();
const server = require('http').Server(app);

const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

UsersSubject.void();
QuizSubject.void();
ChatSubject.void();
CompassSubject.void();
MatchesSubject.void();
AuthSubject.void();
OffersSubject.void();
PostsSubject.void();
MessengerSubject.void();
GlobalSubject.void();
PushNotificationsSubject.void();

server.listen(+PORT, '0.0.0.0', (err: any) => {
  if (err)
    console.log(err.message);
  else
    console.log(`Server started on port ${ PORT }!`);
});
socketInit(server);
